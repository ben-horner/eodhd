package bkh.eodhd.client

import java.io.InputStream
import java.net.{URL, URLConnection, URLEncoder}

import com.fasterxml.jackson.databind.JsonNode

import scala.jdk.CollectionConverters._
import org.apache.log4j.Logger

object UrlBuilder {

  val logger = Logger.getLogger("UrlBuilder")

  def fromUrl(url: String): UrlBuilder = fromUrl(new URL(url))

  def fromUrl(url: URL): UrlBuilder = {
    val path: List[String] = Option(url.getPath)
      .map { p =>
        require(p.startsWith("/"), "url query path did not begin with /")
        p.drop(1).split("/", -1).toList
      }.getOrElse(List())
    val query: Map[String, String] = Option(url.getQuery)
      .map{ q =>
        q.split("&", -1)
          .map { kv =>
            require(kv.contains('='), "url query key-value pair (& delimited) did not contain '='")
            val Array(k, v) = kv.split("=", -1)
            (k, v)
          }.toMap
      }.getOrElse(Map())

    new UrlBuilder(
      url.getProtocol,
      url.getHost,
      if (url.getPort == -1) None else Some(url.getPort),
      path,
      query
    )
  }

}

case class UrlBuilder(protocol: String,
                      host: String,
                      port: Option[Int],
                      path: List[String],
                      query: Map[String, String]) {
  def protocol(protocol: String): UrlBuilder = this.copy(protocol = protocol)
  def host(host: String): UrlBuilder = this.copy(host = host)
  def port(port: Int): UrlBuilder = this.copy(port = Some(port))
  def path(path: List[String]): UrlBuilder = this.copy(path = path)
  def addSegment(segment: String): UrlBuilder = this.copy(path = path :+ segment)
  def query(query: Map[String, String]): UrlBuilder = this.copy(query = query)
  def putParam(key: String, value: String): UrlBuilder = this.copy(query = query + (key -> value))
  def removeParam(key: String): UrlBuilder = this.copy(query = query - key)

  def url(): URL = {
    val portStr: String = port.map(p => s":$p").getOrElse("")
    val pathStr: String  = if (path.isEmpty && query.isEmpty) {
      ""
    } else if (path.isEmpty) {
      "/"
    } else {
      path.map(seg => URLEncoder.encode(seg, "UTF-8")).mkString("/", "/", "/")
    }
    val queryStr: String = if (query.isEmpty) {
      ""
    } else {
      query.map{ case (k, v) =>
        val encodedK = URLEncoder.encode(k, "UTF-8")
        val encodedV = URLEncoder.encode(v, "UTF-8")
        s"$encodedK=$encodedV"
      }.mkString("?", "&", "")
    }
    new URL(s"$protocol://$host$portStr$pathStr$queryStr")
  }

  def jsonContent: (Map[String, Seq[String]], JsonNode) = {
    val connection: URLConnection = this.url().openConnection()
    val headers: Map[String, Seq[String]] = connection.getHeaderFields().asScala.map{ case (k, v) =>
      k -> v.asScala.toSeq
    }.toMap

    import bkh.eodhd.client.ObjectMapperSingleton.mapper
    val is: InputStream = connection.getInputStream()
    val json: JsonNode = try {
      mapper.readTree(is)
    } finally {
      is.close()
    }
    (headers, json)
  }

//  def csvContent: List[List[String]] = {
//    val is: InputStream = this.url().openConnection().getInputStream()
//    val reader: BufferedReader = new BufferedReader(new InputStreamReader(is));
//    val parser: CSVParser = CSVParser.parse(reader, CSVFormat.DEFAULT)
//    val records: List[List[String]] = parser.getRecords.asScala.toList.map{ csvr =>
//      csvr.iterator().asScala.toList
//    }
//    parser.close();
//    records
//  }

}
