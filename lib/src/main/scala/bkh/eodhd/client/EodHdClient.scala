package bkh.eodhd.client

import java.time.LocalDate
import java.time.format.DateTimeFormatter

import bkh.Utilities._
import bkh.eodhd.client.request._
import com.google.common.util.concurrent.RateLimiter
import org.apache.log4j.Logger

object EodHdClient {
  val apiRoot: UrlBuilder = UrlBuilder.fromUrl("https://eodhistoricaldata.com/api")

  val exchangeDetailsPath = "exchange-details"

  val callsPerMinuteLimit = 1000

  val logger = Logger.getLogger("EodHdClient")
}

case class EodHdClient(apiToken: String) {
  import EodHdClient._

  val apiRoot = EodHdClient.apiRoot.putParam("api_token", apiToken)
  // limited to 1K requests per minute
  // limited to 100K requests per day -- ignoring for now...
  val rateLimiter = RateLimiter.create(callsPerMinuteLimit/60)

  // it appears this call costs 0 api calls!
  def user(): UserRequest = {
    UserRequest(this)
  }

  // costs 1 api call
  def exchanges(): ExchangeRequest = {
    ExchangeRequest(this)
  }

  // exchangeId needs to be a value from EodHdExchange.exchangeId
  // exchangeId will often not match ApiExchangeSymbol.exchange in the returned data
  // costs 1 api call
  def symbols(exchangeId: String): SymbolRequest = {
    SymbolRequest(this).exchangeId(exchangeId)
  }

  // all history for a single symbol
  def eodBySymbol(exchangeId: String, symbol: String): EodRequest = {
    EodRequest(this).exchangeId(exchangeId).symbol(symbol)
  }

  // all history for a single bond symbol
  def bondEodBySymbol(symbol: String): EodBondRequest = {
    EodBondRequest(this).symbol(symbol)
  }

  // all symbols for a single day for a single exchange
  def eodByDate(exchangeId: String, date: String): EodBulkRequest = {
    EodBulkRequest(this).exchangeId(exchangeId).date(date)
  }

  // all symbols for a single day for a single exchange
  def eodByDate(exchangeId: String, date: LocalDate): EodBulkRequest = {
    eodByDate(exchangeId, date.format(DateTimeFormatter.ISO_LOCAL_DATE))
  }

  def outstandingShares(exchangeId: String, symbol: String): OutstandingSharesRequest = {
    OutstandingSharesRequest(this).exchangeId(exchangeId).symbol(symbol)
  }



/*
  interested in data that can give me a state at a historical point in time
  that means a snapshot of now is not useful
  a snapshot from the past with a date on it is useful
  a snapshot of now which lets me get transitively to snapshots with dates is a necessary evil  :)

  entities (underlying not from the perspective of the api)
  --------
  exchange
  symbol
  date
  institution
  country


  https://eodhistoricaldata.com/api/
    exchanges-list        x implemented
    exchange-symbol-list  x implemented
    eod                   x implemented
    eod-bulk-last-day     x implemented

    fundamentals          x implemented outstanding shares
    bond-fundamentals
    bulk-fundamentals

    options
    div
    splits
    shorts

    exchange-details  // holidays (2 months back, 6 forward), trading hours & days, active tickers (last 2 months), updated tickers (current day)

    calendar

    intraday
    real-time

    technical
    table.csv
    macro-indicator
    screener
    search


  https://eodhistoricaldata.com/financial-apis/technical-indicators-api/
    technical/AAPL.US?api_token=YOUR_API_KEY&order=d&fmt=json&from=2017-08-01&to=2020-01-01&function=sma&period=50
    technical/AAPL?api_token=YOUR_API_TOKEN&fmt=json&function=ema&filter=last_ema
  https://eodhistoricaldata.com/financial-apis/intraday-historical-data-api/
    intraday/AAPL.US?api_token={your_api_key}
    intraday/AAPL.US?api_token=OeAFFmMliFG5orCUuwAKQ8l4WWFQ67YX&interval=5m
  https://eodhistoricaldata.com/financial-apis/stock-options-data/
    options/AAPL.US?api_token={your_api_key}
    options/AAPL.US?api_token={your_api_key}&from=2018-05-01&to=2018-06-01
    options/AAPL.US?api_token=OeAFFmMliFG5orCUuwAKQ8l4WWFQ67YX
    options/AAPL?contract_name=AAPL180420P00002500&api_token=OeAFFmMliFG5orCUuwAKQ8l4WWFQ67YX&from=2000-01-01
  https://eodhistoricaldata.com/financial-apis/live-realtime-stocks-api/
    real-time/AAPL.US?api_token=OeAFFmMliFG5orCUuwAKQ8l4WWFQ67YX&fmt=json
    real-time/AAPL.US?api_token=OeAFFmMliFG5orCUuwAKQ8l4WWFQ67YX&fmt=json&s=VTI,EUR.FOREX
    real-time/AAPL.US?api_token=OeAFFmMliFG5orCUuwAKQ8l4WWFQ67YX&fmt=json&filter=close
    =WEBSERVICE("https://eodhistoricaldata.com/api/real-time/AAPL.US?api_token=OeAFFmMliFG5orCUuwAKQ8l4WWFQ67YX&fmt=json&filter=close")
  https://eodhistoricaldata.com/financial-apis/api-splits-dividends/
    div/AAPL.US?api_token=OeAFFmMliFG5orCUuwAKQ8l4WWFQ67YX&from=2000-01-01
    splits/AAPL.US?api_token=OeAFFmMliFG5orCUuwAKQ8l4WWFQ67YX&from=2000-01-01
    shorts/AAPL.US?api_token=OeAFFmMliFG5orCUuwAKQ8l4WWFQ67YX&from=2000-01-01
  https://eodhistoricaldata.com/financial-apis/api-for-historical-data-and-volumes/
    eod/MCD.US?api_token={your_api_key}
    eod/MCD.US?api_token=OeAFFmMliFG5orCUuwAKQ8l4WWFQ67YX&period=d
    eod/MCD.US?from=2020-01-05&to=2020-02-10&api_token=OeAFFmMliFG5orCUuwAKQ8l4WWFQ67YX&period=d
    eod/MCD.US?api_token=OeAFFmMliFG5orCUuwAKQ8l4WWFQ67YX&fmt=json&filter=last_close
    table.csv
    table.csv?s=MCD.US&api_token=OeAFFmMliFG5orCUuwAKQ8l4WWFQ67YX&a=05&b=01&c=2017&d=10&e=02&f=2017&g=d
    eod/MCD.US?from=2017-01-05&to=2017-02-10&api_token=OeAFFmMliFG5orCUuwAKQ8l4WWFQ67YX&period=d&fmt=json
  https://eodhistoricaldata.com/financial-apis/macroeconomics-data-and-macro-indicators-api/
    macro-indicator/COUNTRY?api_token=YOUR_API_TOKEN&fmt=json&indicator=inflation_consumer_prices_annual
  https://eodhistoricaldata.com/financial-apis/economic-data-api/
    eod/UK10Y.GBOND?api_token=YOUR_API_TOKEN
    eod/EURIBOR3M.MONEY?api_token=YOUR_API_TOKEN
    eod/LIBOREUR2M.MONEY?api_token=YOUR_API_TOKEN
    eod/STIBOR1M.MONEY?api_token=YOUR_API_TOKEN
    eod/ECBEURUSD.MONEY?api_token=YOUR_API_TOKEN
    eod/NORGEUSDNOK.MONEY?api_token=YOUR_API_TOKEN
  https://eodhistoricaldata.com/financial-apis/bonds-fundamentals-and-historical-api/
    bond-fundamentals/910047AG4?api_token=YOUR_API_TOKEN
    eod/US910047AG49.BOND?api_token=YOUR_API_TOKEN&order=d&fmt=json&from=2017-08-01
  https://eodhistoricaldata.com/financial-apis/calendar-upcoming-earnings-ipos-and-splits/
    calendar/earnings
    calendar/earnings?api_token=YOUR_API_KEY&fmt=json&from=2018-12-02&to=2018-12-06
    calendar/earnings?api_token=YOUR_API_KEY&fmt=json&symbols=AAPL.US,MSFT.US,AI.PA&from=2018-01-01
    calendar/trends
    calendar/trends?api_token=YOUR_API_KEY&fmt=json&symbols=AAPL.US,MSFT.US,AI.PA
    calendar/ipos
    calendar/ipos?api_token=YOUR_API_TOKEN&fmt=json&from=2018-12-02&to=2018-12-06
    calendar/splits
    calendar/splits?api_token=YOUR_API_TOKEN&fmt=json&from=2018-12-02&to=2018-12-06
  https://eodhistoricaldata.com/financial-apis/stock-etfs-fundamental-data-feeds/
    fundamentals/AAPL.US?api_token={your_api_key}
    fundamentals/AAPL.US?api_token=OeAFFmMliFG5orCUuwAKQ8l4WWFQ67YX
    fundamentals/AAPL.US?api_token=OeAFFmMliFG5orCUuwAKQ8l4WWFQ67YX&filter=General
    fundamentals/AAPL.US?api_token=OeAFFmMliFG5orCUuwAKQ8l4WWFQ67YX&filter=General::Code
    fundamentals/AAPL.US?api_token=OeAFFmMliFG5orCUuwAKQ8l4WWFQ67YX&filter=Financials::Balance_Sheet::yearly
    fundamentals/AAPL.US?api_token=OeAFFmMliFG5orCUuwAKQ8l4WWFQ67YX&filter=General::Code,General,Earnings
    fundamentals/VTI.US?api_token=OeAFFmMliFG5orCUuwAKQ8l4WWFQ67YX
    fundamentals/GSPC.INDX?api_token=YOUR_API_KEY_HER
    bulk-fundamentals/NASDAQ?api_token={YOUR_API_TOKEN}
    bulk-fundamentals/NASDAQ?api_token={YOUR_API_TOKEN}&fmt=json
    bulk-fundamentals/NASDAQ?api_token=YOURAPIKEY&offset=500&limit=100
    bulk-fundamentals/NASDAQ?&symbols=AAPL.US,MSFT.US&api_token={YOUR_API_TOKEN}
    things with dates:
      * SplitsDividends::NumberDividendsByYear
      * outstandingShares::quarterly
      * Earnings::History
      * Financials::Balance_Sheet::quarterly
      * Financials::Cash_Flow::quarterly
      * Financials::Income_Statement::quarterly
    relationship snapshots:
      * Holders::Institutions
      * Holders::Funds
  https://eodhistoricaldata.com/financial-apis/screener-api/
    screener?api_token=YOUR_API_TOKEN&sort=market_capitalization.desc&filters=[["market_capitalization",">",1000],["name","match","apple"],["code","=","AAPL"],["exchange","=","us"],["sector","=","Technology"]]&limit=10&offset=0
  https://eodhistoricaldata.com/financial-apis/exchanges-api-trading-hours-and-holidays/
    exchange-details/EXCHANGE_CODE?api_token=YOUR_API_TOKEN
    exchange-details/EXCHANGE_CODE?api_token=YOUR_API_TOKEN
  https://eodhistoricaldata.com/financial-apis/exchanges-api-list-of-tickers-and-trading-hours/
    exchanges-list/?api_token=YOUR_API_TOKEN&fmt=json
    exchange-symbol-list/{EXCHANGE_CODE}?api_token={YOUR_API_KEY}
  https://eodhistoricaldata.com/financial-apis/search-api-for-stocks-etfs-mutual-funds-and-indices/
    search/{query_string}
    search/AAPL?api_token=YOUR_API_TOKEN
    search/Apple Inc?api_token=YOUR_API_TOKEN
    search/Apple Inc?api_token=YOUR_API_TOKEN&limit=1
  https://eodhistoricaldata.com/financial-apis/bulk-api-eod-splits-dividends/
    eod-bulk-last-day/US?api_token={YOUR_API_KEY}
    eod-bulk-last-day/US?api_token={YOUR_API_KEY}&type=splits
    eod-bulk-last-day/US?api_token={YOUR_API_KEY}&type=dividends
    eod-bulk-last-day/US?api_token={YOUR_API_KEY}&date=2010-09-21
    eod-bulk-last-day/US?api_token={YOUR_API_KEY}&symbols=MSFT,AAPL,BMW.XETRA,SAP.F
    eod-bulk-last-day/US?api_token={YOUR_API_KEY}&symbols=MSFT,AAPL&fmt=json
    eod-bulk-last-day/US?api_token={YOUR_API_KEY}&symbols=MSFT,AAPL&fmt=json&filter=extended

  https://eodhistoricaldata.com/financial-apis/curl-wget-stock-api-examples/
    curl https://eodhistoricaldata.com/api/eod/AAPL.US?api_token=OeAFFmMliFG5orCUuwAKQ8l4WWFQ67YX&order=d&fmt=json&from=2017-08-01
    curl https://eodhistoricaldata.com/api/fundamentals/AAPL.US?api_token=OeAFFmMliFG5orCUuwAKQ8l4WWFQ67YX
    wget https://eodhistoricaldata.com/api/eod/AAPL.US?api_token=OeAFFmMliFG5orCUuwAKQ8l4WWFQ67YX&order=d&fmt=json&from=2017-08-01
    wget https://eodhistoricaldata.com/api/fundamentals/AAPL.US?api_token=OeAFFmMliFG5orCUuwAKQ8l4WWFQ67YX


*/

  def macroIndicator(): Unit = ???

  def fundamentals(): Unit = ???
  def bulkFundamentals(): Unit = ???
  def bondFundamentals(): Unit = ???

  def div(): Unit = ???
  def splits(): Unit = ???
  def shorts(): Unit = ???

  def calendar(): Unit = ???
  def options(): Unit = ???

  def intraday(): Unit = ???
  def realTime(): Unit = ???


  def screener(): Unit = ???
  def search(): Unit = ???

  def technicalIndicator(): Unit = ???

}
