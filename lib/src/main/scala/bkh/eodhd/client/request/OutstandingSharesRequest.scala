package bkh.eodhd.client.request

import bkh.api.entities.EodHdOutstandingShares
import bkh.eodhd.client.{EodHdClient, ObjectMapperSingleton, UrlBuilder}
import org.apache.log4j.Logger

import scala.jdk.CollectionConverters._
import bkh.Utilities.time

object OutstandingSharesRequest {
  val fundamentalsPath = "fundamentals"
  val logger = Logger.getLogger("OutstandingSharesRequest")
}

case class OutstandingSharesRequest(eodHdClient: EodHdClient,
                                    exchangeId: Option[String] = None,
                                    symbol: Option[String] = None) {
  import OutstandingSharesRequest.{fundamentalsPath, logger}
  def exchangeId(exchangeId: String): OutstandingSharesRequest = this.copy(exchangeId = Some(exchangeId))
  def symbol(symbol: String): OutstandingSharesRequest = this.copy(symbol = Some(symbol))

  def get(): Seq[EodHdOutstandingShares] = {
    val api = urlBuilder()

    val wait = eodHdClient.rateLimiter.acquire(1)
    time { s =>
      logger.debug(s"OutstandingSharesRequest.get(${exchangeId.get}, ${symbol.get}).get() waited $wait seconds, then took $s s")
    } {
      val (headers, jsonResult) = api.jsonContent
      logger.info(s"headers: X-RateLimit-Remaining=${headers("X-RateLimit-Remaining")}")
      assume(jsonResult.isArray, s"$fundamentalsPath json result was not json array")
      import ObjectMapperSingleton.mapper
      val outstandingShares = jsonResult.iterator().asScala.map { jsonNode =>
        assume(jsonNode.isObject, s"$fundamentalsPath element was not json object")
        val fromApi = try {
          mapper.convertValue(jsonNode, classOf[EodHdOutstandingShares])
        } catch {
          case e: Exception =>
            val errorMsg = s"url returned json which couldn't be converted to EodHdOutstandingShares; ${api.url()} => ${mapper.writeValueAsString(jsonNode)}"
            logger.error(errorMsg, e)
            throw new RuntimeException(errorMsg, e)
        }
        // adding join fields
        fromApi.copy(exchangeId = exchangeId.get, symbol = symbol.get).scrub()
      }.toList
      outstandingShares
    }
  }

  private def urlBuilder(): UrlBuilder = {
    assume(exchangeId.isDefined, """must supply exchangeId -- i.e. eodRequest.exchangeId("US")""")
    assume(symbol.isDefined, """must supply code -- i.e. eodRequest.symbol("AAPL")""")
    val api = eodHdClient.apiRoot
      .addSegment(fundamentalsPath)
      .addSegment(s"${symbol.get}.${exchangeId.get}")
      .putParam("filter", "outstandingShares::quarterly")
      .putParam("fmt", "json")
    api
  }
  // time series from fundamentals:
  // SplitsDividends::NumberDividendsByYear
  // Earnings::History {epsActual, epsEstimate, epsDifference, surprisePercent}
  // Financials::Balance_sheet::quarterly {assets, liabilities, earnings, debt, investments, receivables, payable, stock, outstanding}
  // Financials::Cash_Flow::quarterly {}
  // Financials::Income_Statement::quarterly {}

}
