package bkh.eodhd.client.request

import java.time.LocalDate
import java.time.format.DateTimeFormatter

import bkh.api.entities.EodHdEndOfDay
import bkh.eodhd.client.{EodHdClient, ObjectMapperSingleton, UrlBuilder}
import org.apache.log4j.Logger

import scala.jdk.CollectionConverters._
import bkh.Utilities.time

object EodRequest{
  val eodPath = "eod"

  sealed abstract class Period(val name: String)
  object Period {
    case object Daily extends Period("d")
    case object Weekly extends Period("w")
    case object Monthly extends Period("m")
    val values = Seq(Daily, Weekly, Monthly)
  }

  sealed abstract class Order(val name: String)
  object Order {
    case object Ascending extends Order("a")
    case object Descending extends Order("d")
    val values = Seq(Ascending, Descending)
  }

  val logger = Logger.getLogger("EodRequest")

}

case class EodRequest(eodHdClient: EodHdClient,
                      exchangeId: Option[String] = None,
                      symbol: Option[String] = None,
                      period: Option[EodRequest.Period] = None,
                      order: Option[EodRequest.Order] = None,
                      from: Option[LocalDate] = None,
                      to: Option[LocalDate] = None) {
  import EodRequest.Order._
  import EodRequest.Period._
  import EodRequest.{eodPath, logger}
  def exchangeId(exchangeId: String): EodRequest = this.copy(exchangeId = Some(exchangeId))
  def symbol(symbol: String): EodRequest = this.copy(symbol = Some(symbol))
  def daily(): EodRequest = this.copy(period = Some(Daily))
  def weekly(): EodRequest = this.copy(period = Some(Weekly))
  def monthly(): EodRequest = this.copy(period = Some(Monthly))
  def ascending(): EodRequest = this.copy(order = Some(Ascending))
  def descending(): EodRequest = this.copy(order = Some(Descending))
  def from(date: LocalDate): EodRequest = this.copy(from = Some(date))
  def to(date: LocalDate): EodRequest = this.copy(to = Some(date))
  def from(date: String): EodRequest = from(LocalDate.parse(date, DateTimeFormatter.ISO_LOCAL_DATE))
  def to(date: String): EodRequest = to(LocalDate.parse(date, DateTimeFormatter.ISO_LOCAL_DATE))

  def get(): Seq[EodHdEndOfDay] = {
    assume(exchangeId != "BOND")
    val api = urlBuilder()

    val wait = eodHdClient.rateLimiter.acquire(1)
    time { s =>
      logger.debug(s"EodRequest(${exchangeId.get}, ${symbol.get}).get() waited $wait seconds, then took $s s")
    } {
      val (headers, jsonResult) = api.jsonContent
      logger.info(s"headers: X-RateLimit-Remaining=${headers("X-RateLimit-Remaining")}")
      assume(jsonResult.isArray, s"$eodPath json result was not json array")
      import ObjectMapperSingleton.mapper
      val eods = jsonResult.iterator().asScala.map { jsonNode =>
        assume(jsonNode.isObject, s"$eodPath element was not json object")
        val fromApi = try {
          mapper.convertValue(jsonNode, classOf[EodHdEndOfDay])
        } catch {
          case e: Exception =>
            val errorMsg = s"url returned json which couldn't be converted to EodHdEndOfDay; ${api.url()} => ${mapper.writeValueAsString(jsonNode)}"
            logger.error(errorMsg, e)
            throw new RuntimeException(errorMsg, e)
        }
        // adding join fields
        fromApi.copy(exchangeId = exchangeId.get, symbol = symbol.get).scrub()
      }.toList
      eods
    }
  }

  private def urlBuilder(): UrlBuilder = {
    assume(exchangeId.isDefined, """must supply exchangeId -- i.e. eodRequest.exchangeId("US")""")
    assume(symbol.isDefined, """must supply code -- i.e. eodRequest.symbol("AAPL")""")
    var api = eodHdClient.apiRoot
      .addSegment(eodPath)
      .addSegment(s"${symbol.get}.${exchangeId.get}")
      .putParam("fmt", "json")
    if (period.isDefined) {
      api = api.putParam("period", period.get.name)
    }
    if (order.isDefined) {
      api = api.putParam("order", order.get.name)
    }
    if (from.isDefined) {
      api = api.putParam("from", from.get.format(DateTimeFormatter.ISO_LOCAL_DATE))
    }
    if (to.isDefined) {
      api = api.putParam("to", to.get.format(DateTimeFormatter.ISO_LOCAL_DATE))
    }
    api
  }

}
