package bkh.eodhd.client.request

import bkh.api.entities.EodHdSymbol
import bkh.eodhd.client.{EodHdClient, ObjectMapperSingleton, UrlBuilder}
import org.apache.log4j.Logger

import scala.jdk.CollectionConverters._
import bkh.Utilities.time

object SymbolRequest{
  val exchangeSymbolListPath = "exchange-symbol-list"
  val logger = Logger.getLogger("SymbolRequest")
}

case class SymbolRequest(eodHdClient: EodHdClient,
                         exchangeId: Option[String] = None) {
  import SymbolRequest.{exchangeSymbolListPath, logger}
  def exchangeId(exchangeId: String): SymbolRequest = this.copy(exchangeId = Some(exchangeId))

  def get(): Seq[EodHdSymbol] = {
    val api = urlBuilder()

    val wait = eodHdClient.rateLimiter.acquire(1)
    time { s =>
      logger.debug(s"SymbolRequest(${exchangeId.get}).get() waited $wait seconds, then took $s s")
    } {
      val (headers, jsonResult) = api.jsonContent
      logger.info(s"headers: X-RateLimit-Remaining=${headers("X-RateLimit-Remaining")}")
      assume(jsonResult.isArray, s"$exchangeSymbolListPath json result was not json array")
      import ObjectMapperSingleton.mapper
      val symbols = jsonResult.iterator().asScala.map { jsonNode =>
        assume(jsonNode.isObject, s"$exchangeSymbolListPath element was not json object")
        val fromApi = try {
          mapper.convertValue(jsonNode, classOf[EodHdSymbol])
        } catch {
          case e: Exception =>
            val errorMsg = s"url returned json which couldn't be converted to EodHdSymbol; ${api.url()} => ${mapper.writeValueAsString(jsonNode)}"
            logger.error(errorMsg, e)
            throw new RuntimeException(errorMsg, e)
        }
        // sadly the api doesn't bring back the "exchange id" which is required to query eod data...
        fromApi.copy(exchangeId = exchangeId.get).scrub()
      }.toSeq
      //    logger.debug(s"api symbols for $exchangeId")
      //    symbols.foreach(s =>logger.debug(s"\t$s"))
      symbols
    }
  }

  private def urlBuilder(): UrlBuilder = {
    assume(exchangeId.isDefined, """must supply exchangeId -- i.e. symbolRequest.exchangeId("US")""")
    val api = eodHdClient.apiRoot
      .addSegment(SymbolRequest.exchangeSymbolListPath)
      .addSegment(s"${exchangeId.get}")
      .putParam("fmt", "json")
    api
  }

}
