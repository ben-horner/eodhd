package bkh.eodhd.client.request

import java.time.LocalDate
import java.time.format.DateTimeFormatter

import bkh.Utilities.time
import bkh.api.entities.EodHdBondEndOfDay
import bkh.eodhd.client.{EodHdClient, ObjectMapperSingleton, UrlBuilder}
import org.apache.log4j.Logger

import scala.jdk.CollectionConverters._


object EodBondRequest {

  val logger = Logger.getLogger("EodBondRequest")

}

case class EodBondRequest(eodHdClient: EodHdClient,
                          symbol: Option[String] = None,
                          period: Option[EodRequest.Period] = None,
                          order: Option[EodRequest.Order] = None,
                          from: Option[LocalDate] = None,
                          to: Option[LocalDate] = None) {
  import EodRequest.Order._
  import EodRequest.Period._
  import EodRequest.eodPath
  import EodBondRequest.logger
  def symbol(symbol: String): EodBondRequest = this.copy(symbol = Some(symbol))
  def daily(): EodBondRequest = this.copy(period = Some(Daily))
  def weekly(): EodBondRequest = this.copy(period = Some(Weekly))
  def monthly(): EodBondRequest = this.copy(period = Some(Monthly))
  def ascending(): EodBondRequest = this.copy(order = Some(Ascending))
  def descending(): EodBondRequest = this.copy(order = Some(Descending))
  def from(date: LocalDate): EodBondRequest = this.copy(from = Some(date))
  def to(date: LocalDate): EodBondRequest = this.copy(to = Some(date))
  def from(date: String): EodBondRequest = from(LocalDate.parse(date, DateTimeFormatter.ISO_LOCAL_DATE))
  def to(date: String): EodBondRequest = to(LocalDate.parse(date, DateTimeFormatter.ISO_LOCAL_DATE))

  def get(): Seq[EodHdBondEndOfDay] = {
    val api = urlBuilder()

    val wait = eodHdClient.rateLimiter.acquire(1)
    time { s =>
      logger.debug(s"EodBondRequest(${symbol.get}).get() waited $wait seconds, then took $s s")
    } {
      val (headers, jsonResult) = api.jsonContent
      logger.info(s"headers: X-RateLimit-Remaining=${headers("X-RateLimit-Remaining")}")
      assume(jsonResult.isArray, s"$eodPath json result was not json array")
      import ObjectMapperSingleton.mapper
      val eods = jsonResult.iterator().asScala.map { jsonNode =>
        assume(jsonNode.isObject, s"$eodPath element was not json object")
        val fromApi = try {
          mapper.convertValue(jsonNode, classOf[EodHdBondEndOfDay])
        } catch {
          case e: Exception =>
            val errorMsg = s"url returned json which couldn't be converted to EodHdBondEndOfDay; ${api.url()} => ${mapper.writeValueAsString(jsonNode)}"
            logger.error(errorMsg, e)
            throw new RuntimeException(errorMsg, e)
        }
        // adding join fields
        fromApi.copy(exchangeId = "BOND", symbol = symbol.get).scrub()
      }.toList
      eods
    }
  }

  private def urlBuilder(): UrlBuilder = {
    assume(symbol.isDefined, """must supply code -- i.e. eodRequest.symbol("AAPL")""")
    var api = eodHdClient.apiRoot
      .addSegment(eodPath)
      .addSegment(s"${symbol.get}.BOND")
      .putParam("fmt", "json")
    if (period.isDefined) {
      api = api.putParam("period", period.get.name)
    }
    if (order.isDefined) {
      api = api.putParam("order", order.get.name)
    }
    if (from.isDefined) {
      api = api.putParam("from", from.get.format(DateTimeFormatter.ISO_LOCAL_DATE))
    }
    if (to.isDefined) {
      api = api.putParam("to", to.get.format(DateTimeFormatter.ISO_LOCAL_DATE))
    }
    api
  }

}
