package bkh.eodhd.client.request

import bkh.api.entities.EodHdExchange
import bkh.eodhd.client.{EodHdClient, ObjectMapperSingleton, UrlBuilder}
import org.apache.log4j.Logger

import scala.jdk.CollectionConverters._
import bkh.Utilities.time

object ExchangeRequest {
  val exchangeListPath = "exchanges-list"
  val logger = Logger.getLogger("ExchangeRequest")
}

case class ExchangeRequest(eodHdClient: EodHdClient) {
  import ExchangeRequest.{exchangeListPath, logger}

  def get(): Seq[EodHdExchange] = {
    val api = urlBuilder()

    val wait = eodHdClient.rateLimiter.acquire(1)
    time { s =>
      logger.debug(s"ExchangeRequest().get waited $wait seconds, then took $s s")
    } {
      val (headers, jsonResult) = api.jsonContent
      logger.info(s"headers: X-RateLimit-Remaining=${headers("X-RateLimit-Remaining")}")
      assume(jsonResult.isArray, s"$exchangeListPath json result was not json array")
      import ObjectMapperSingleton.mapper
      val exchanges = jsonResult.iterator().asScala.map { jsonNode =>
        try {
          mapper.convertValue(jsonNode, classOf[EodHdExchange]).scrub()
        } catch {
          case e: Exception =>
            val errorMsg = s"url returned json which couldn't be converted to EodHdExchange; ${api.url()} => ${mapper.writeValueAsString(jsonNode)}"
            logger.error(errorMsg, e)
            throw new RuntimeException(errorMsg, e)
        }
      }.toSeq
      exchanges
    }
  }

  private def urlBuilder(): UrlBuilder = {
    val api = eodHdClient.apiRoot
      .addSegment(ExchangeRequest.exchangeListPath)
      .putParam("fmt", "json")
    api
  }

}
