package bkh.eodhd.client.request

import java.time.LocalDate
import java.time.format.DateTimeFormatter

import bkh.api.entities.EodHdEndOfDay
import bkh.eodhd.client.{EodHdClient, ObjectMapperSingleton, UrlBuilder}
import org.apache.log4j.Logger

import scala.jdk.CollectionConverters._
import bkh.Utilities.time

object EodBulkRequest{
  val eodBulkPath = "eod-bulk-last-day"
  val logger = Logger.getLogger("EodBulkRequest")
}

case class EodBulkRequest(eodHdClient: EodHdClient,
                          exchangeId: Option[String] = None,
                          date: Option[LocalDate] = None) {
  import EodBulkRequest.{eodBulkPath, logger}
  def exchangeId(exchangeId: String): EodBulkRequest = this.copy(exchangeId = Some(exchangeId))
  def date(date: LocalDate): EodBulkRequest = this.copy(date = Some(date))
  def date(dateStr: String): EodBulkRequest = date(LocalDate.parse(dateStr, DateTimeFormatter.ISO_LOCAL_DATE))

  def get(): Seq[EodHdEndOfDay] = {
    val api = urlBuilder()

    val wait = eodHdClient.rateLimiter.acquire(100)
    time { s =>
      logger.debug(s"EodBulkRequest.get(${exchangeId.get}, ${date.get}).get() waited $wait seconds, then took $s s")
    } {
      val (headers, jsonResult) = api.jsonContent
      logger.info(s"headers: X-RateLimit-Remaining=${headers("X-RateLimit-Remaining")}")
      assume(jsonResult.isArray, s"$eodBulkPath json result was not json array")
      import ObjectMapperSingleton.mapper
      val eods = jsonResult.iterator().asScala.map { jsonNode =>
        assume(jsonNode.isObject, s"$eodBulkPath element was not json object")
        val fromApi = try {
          mapper.convertValue(jsonNode, classOf[EodHdEndOfDay])
        } catch {
          case e: Exception =>
            val errorMsg = s"url returned json which jackson could not convert to EodHdEndOfDay; ${api.url()} => ${mapper.writeValueAsString(jsonNode)}"
            logger.error(errorMsg, e)
            throw new RuntimeException(errorMsg, e)
        }
        fromApi.scrub()
      }.toList
      eods
    }
  }

  private def urlBuilder(): UrlBuilder = {
    assume(exchangeId.isDefined, """must supply exchangeId -- i.e. eodBulkRequest.exchangeId("US")""")
    var api = eodHdClient.apiRoot
      .addSegment(EodBulkRequest.eodBulkPath)
      .addSegment(s"${exchangeId.get}")
      .putParam("fmt", "json")
    if (date.isDefined) {
      api = api.putParam("date", date.get.format(DateTimeFormatter.ISO_LOCAL_DATE))
    }
    api
  }

}
