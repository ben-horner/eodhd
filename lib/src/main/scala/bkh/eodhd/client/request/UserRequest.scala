package bkh.eodhd.client.request

import bkh.api.entities.EodHdUser
import bkh.eodhd.client.{EodHdClient, ObjectMapperSingleton, UrlBuilder}
import org.apache.log4j.Logger
import bkh.Utilities.time

object UserRequest {
  val userPath = "user"
  val logger = Logger.getLogger("UserRequest")
}

case class UserRequest(eodHdClient: EodHdClient) {
  import UserRequest.{logger, userPath}

  def get(): EodHdUser = time { s =>
    logger.debug(s"UserRequest.get() took $s s")
  }{
    val api = urlBuilder()

    logger.info("api client user()")
    val (headers, jsonResult) = api.jsonContent
    logger.info(s"headers: X-RateLimit-Remaining=${headers("X-RateLimit-Remaining")}")
    assume(jsonResult.isObject, s"$userPath json result was not json object")
    import ObjectMapperSingleton.mapper
    val user = try {
      mapper.convertValue(jsonResult, classOf[EodHdUser])
    } catch {
      case e: Exception =>
        val errorMsg = s"url returned json which couldn't be converted to EodHdUser; ${api.url()} => ${mapper.writeValueAsString(jsonResult)}"
        logger.error(errorMsg, e)
        throw new RuntimeException(errorMsg, e)
    }
    user
  }

  private def urlBuilder(): UrlBuilder = {
    val api = eodHdClient.apiRoot
      .addSegment(UserRequest.userPath)
    api
  }

}
