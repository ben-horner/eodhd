package bkh.eodhd.client

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule

object ObjectMapperSingleton {
  val mapper: ObjectMapper = {
    val result = new ObjectMapper()
    result.registerModule(DefaultScalaModule)
    result
  }
}
