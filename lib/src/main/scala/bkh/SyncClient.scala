package bkh

import java.time.{Duration, LocalDate, LocalDateTime}
import java.time.format.DateTimeFormatter
import java.util.concurrent.{Callable, ExecutorService, Executors}

import bkh.Utilities._
import bkh.api.entities.{Diffable, EodHdBondEndOfDay, EodHdEndOfDay, EodHdExchange, EodHdOutstandingShares, EodHdSymbol, SyncEndOfDayByDate, SyncEndOfDayBySymbol, SyncExchangeList, SyncSymbolList}
import bkh.db.client.DbClient
import bkh.eodhd.client.EodHdClient
import org.apache.log4j.Logger

import scala.jdk.CollectionConverters._
import scala.util.Try

object SyncClient {

  val logger: Logger = Logger.getLogger("SyncClient")
}

class SyncClient(eodHdClient: EodHdClient, dbClient: DbClient, numThreads: Int) {

  import SyncClient.logger

  private val executorService: ExecutorService = Executors.newFixedThreadPool(numThreads)

  def close(): Unit = {
    dbClient.close()
    val incomplete = executorService.shutdownNow()
    if (incomplete.size() > 0) {
      logger.error(s"SyncClient.close() with ${incomplete.size()} Runnables still running")
    }
  }

  def syncExchanges(): Unit = {
    var apiTime = 0.0
    val apiExchanges: Seq[EodHdExchange] = time(s => apiTime = s) {
      eodHdClient.exchanges().get()
    }
    time { s =>
      logger.info(s"SyncClient.syncExchanges() took $apiTime s retrieving from api, then took $s s")
    } {
      val dbExchanges: Seq[EodHdExchange] = dbClient.exchanges()

      val diff = Diffable.diff[String, EodHdExchange](dbExchanges, apiExchanges, e => e.key())
      logger.info(s"exchange sync summary: ${dbExchanges.size} from db, ${apiExchanges.size} from api, ${diff.changed.size} changed, ${diff.removed.size} removed, ${diff.added.size} added, ${diff.same.size} same")

      val keyToSyncing = dbClient.exchangeListSyncing().map(es => es.key() -> es).toMap
      val today = LocalDate.now().format(DateTimeFormatter.ISO_LOCAL_DATE)

      for ((dbExchange, apiExchange) <- diff.changed) { // (in db) and (in api) and (different)
        logger.info(s"changed for ${dbExchange.exchangeId}: ${dbExchange.diff(apiExchange)}")
        val prevSyncing = keyToSyncing(dbExchange.key())
        val nextSyncing = prevSyncing.copy(
          lastUpdated = today,
          lastSeen = today,
          lastReappeared =
            if (prevSyncing.lastAbsent != null && prevSyncing.lastSeen < prevSyncing.lastAbsent) today
            else prevSyncing.lastReappeared
        )
        apiExchange.insert(dbClient, true)
        nextSyncing.upsert(dbClient)
      }
      for (exchange <- diff.removed) { // (in db) and (not in api)
        val prevSyncing = keyToSyncing(exchange.key())
        prevSyncing.copy(lastAbsent = today).upsert(dbClient)
      }
      for (exchange <- diff.added) { // (not in db) and (in api)
        exchange.insert(dbClient)
        SyncExchangeList(exchange.exchangeId, today, today, today, null, null).upsert(dbClient)
      }
      for (exchange <- diff.same) { // (in db) and (in api)
        val prevSyncing = keyToSyncing(exchange.key())
        val nextSyncing = prevSyncing.copy(
          lastSeen = today,
          lastReappeared =
            if (prevSyncing.lastAbsent != null && prevSyncing.lastSeen < prevSyncing.lastAbsent) today
            else prevSyncing.lastReappeared
        )
        nextSyncing.upsert(dbClient)
      }
    }
  }

  def syncSymbols(exchangeId: String): Unit = {
    var apiTime = 0.0
    val apiSymbols: Seq[EodHdSymbol] = time(s => apiTime = s) {
      eodHdClient.symbols(exchangeId).get()
    }
    time { s =>
      logger.info(s"SyncClient.syncSymbols($exchangeId) took $apiTime s retrieving from api, then took $s s")
    } {
      val dbSymbols: Seq[EodHdSymbol] = dbClient.symbols(exchangeId)

      val diff = Diffable.diff[(String, String), EodHdSymbol](dbSymbols, apiSymbols, s => s.key())
      logger.info(s"symbol sync summary for $exchangeId: ${dbSymbols.size} from db, ${apiSymbols.size} from api, ${diff.changed.size} changed, ${diff.removed.size} removed, ${diff.added.size} added, ${diff.same.size} same")

      val keyToSyncing = dbClient.symbolListSyncing(exchangeId).map(s => (s.exchangeId, s.symbol) -> s).toMap
      val today = LocalDate.now().format(DateTimeFormatter.ISO_LOCAL_DATE)

      for ((dbSymbol, apiSymbol) <- diff.changed) { // (in db) and (in api) and (different)
        logger.info(s"changed for (${dbSymbol.exchangeId}, ${dbSymbol.symbol}): ${dbSymbol.diff(apiSymbol)}")
        val prevSyncing = keyToSyncing(dbSymbol.key())
        val nextSyncing = prevSyncing.copy(
          lastUpdated = today,
          lastSeen = today,
          lastReappeared =
            if (prevSyncing.lastAbsent != null && prevSyncing.lastSeen < prevSyncing.lastAbsent) today
            else prevSyncing.lastReappeared
        )
        apiSymbol.insert(dbClient, upsert = true)
        nextSyncing.upsert(dbClient)
      }
      for (symbol <- diff.removed) { // (in db) and (not in api)
        val prevSyncing = keyToSyncing(symbol.key())
        prevSyncing.copy(lastAbsent = today).upsert(dbClient)
      }
      for (symbol <- diff.added) { // (not in db) and (in api)
        symbol.insert(dbClient)
        SyncSymbolList(symbol.exchangeId, symbol.symbol, today, today, today, null, null).upsert(dbClient)
      }
      for (symbol <- diff.same) { // (in db) and (in api)
        val prevSyncing = keyToSyncing(symbol.key())
        val nextSyncing = prevSyncing.copy(
          lastSeen = today,
          lastReappeared =
            if (prevSyncing.lastAbsent != null && prevSyncing.lastSeen < prevSyncing.lastAbsent) today
            else prevSyncing.lastReappeared
        )
        nextSyncing.upsert(dbClient)
      }
    }
  }

  def syncSymbols(): Unit = time { ms =>
    logger.info(s"SyncClient.syncSymbols() took $ms ms")
  } {
    val syncSymbolCallables: Seq[Callable[Try[Boolean]]] = dbClient.exchanges()
      .map { exchange =>
        toCallable(
          //          logExceptions(
          syncSymbols(exchange.exchangeId)
          //          )
        )
      }
    logger.info(s"invoking ${syncSymbolCallables.size} sync exchange")
    invokeAll(syncSymbolCallables)
  }


  def syncEodsBySymbol(exchangeId: String, symbol: String): Unit = {
    var apiTime = 0.0
    val apiEods = time(s => apiTime = s) {
      eodHdClient.eodBySymbol(exchangeId, symbol).get().sortBy(_.date)
    }
    time { s =>
      logger.info(s"SyncClient.syncEodsBySymbol($exchangeId, $symbol) took $apiTime s retrieving from api, then took $s s")
    } {
      val dbEods = dbClient.eodBySymbol(exchangeId, symbol)

      val diff = Diffable.diff[(String, String, String), EodHdEndOfDay](dbEods, apiEods, eod => eod.key())
      logger.info(s"eod sync summary for ($exchangeId, $symbol): ${dbEods.size} from db, ${apiEods.size} from api, ${diff.changed.size} changed, ${diff.removed.size} removed, ${diff.added.size} added, ${diff.same.size} same")

      val timeline = apiEods.map(eod => LocalDate.parse(eod.date, DateTimeFormatter.ISO_LOCAL_DATE).atTime(0, 0))
      var holeSizes = Seq[Int]()
      var contigSizes = Seq[Int]()
      var currentContig = 0
      timeline.sliding(2).dropWhile(_.size < 2).foreach { pair =>
        val d1 = pair.head
        val d2 = pair.drop(1).head
        val hole = Duration.between(d1, d2).toDays.toInt - 1
        if (hole > 0) {
          contigSizes = contigSizes :+ (currentContig + 1)
          currentContig = 0
          holeSizes = holeSizes :+ hole
        } else {
          currentContig = currentContig + 1
        }
      }
      holeSizes = holeSizes.sorted
      contigSizes = contigSizes.sorted
      val meanContig = if (contigSizes.size > 2) {
        1.0 * contigSizes.drop(1).dropRight(1).sum / (contigSizes.size - 2)
      } else if (contigSizes.size > 0) {
        1.0 * contigSizes.sum / contigSizes.size
      } else {
        0.0
      }
      val meanHole = if (holeSizes.size > 2) {
        1.0 * holeSizes.drop(1).dropRight(1).sum / (holeSizes.size - 2)
      } else if (holeSizes.size > 0) {
        1.0 * holeSizes.sum / holeSizes.size
      } else {
        0.0
      }
      val syncSummary = SyncEndOfDayBySymbol(
        exchangeId,
        symbol,
        dateOfCall = LocalDate.now().format(DateTimeFormatter.ISO_LOCAL_DATE),
        deltaChanges = diff.changed.size,
        deltaDatesAdded = diff.added.size,
        deltaDatesMissing = diff.removed.size,
        numDates = apiEods.size,
        minDate = apiEods.headOption.map(_.date).orNull,
        maxDate = apiEods.lastOption.map(_.date).orNull,
        hullSize = if (timeline.isEmpty) 0 else Duration.between(timeline.head, timeline.last).toDays.toInt,
        biggestContiguous = if (contigSizes.isEmpty) 0 else contigSizes.last,
        meanContiguous = meanContig,
        numHoles = holeSizes.size,
        biggestHole = if (holeSizes.isEmpty) 0 else holeSizes.last,
        meanHole = meanHole)

      for ((dbEod, apiEod) <- diff.changed) { // (in db) and (in api) and (different)
        logger.info(s"changed for (${dbEod.exchangeId}, ${dbEod.symbol}, ${dbEod.date}): ${dbEod.diff(apiEod)}")
        apiEod.insert(dbClient, upsert = true)
      }
      for (eod <- diff.removed) { // (in db) and (not in api)
        logger.info(s"end of day removed: $eod")
      }
      for (eod <- diff.added) { // (not in db) and (in api)
        eod.insert(dbClient)
      }
      for (eod <- diff.same) { // (in db) and (in api)
        // unchanged...
      }
      syncSummary.upsert(dbClient)
    }
  }

  def syncBondEodsBySymbol(symbol: String): Unit = {
    var apiTime = 0.0
    val apiBondEods = time(s => apiTime = s) {
      eodHdClient.bondEodBySymbol(symbol).get().sortBy(_.date)
    }
    time { s =>
      logger.info(s"SyncClient.syncBondEodsBySymbol($symbol) took $apiTime s retrieving from api, then took $s s")
    } {
      val dbBondEods = dbClient.bondEodBySymbol(symbol)

      val diff = Diffable.diff[(String, String, String), EodHdBondEndOfDay](dbBondEods, apiBondEods, bondEod => bondEod.key())
      logger.info(s"bond eod sync summary for ($symbol): ${dbBondEods.size} from db, ${apiBondEods.size} from api, ${diff.changed.size} changed, ${diff.removed.size} removed, ${diff.added.size} added, ${diff.same.size} same")

      val timeline = apiBondEods.map(bondEod => LocalDate.parse(bondEod.date, DateTimeFormatter.ISO_LOCAL_DATE).atTime(0, 0))
      var holeSizes = Seq[Int]()
      var contigSizes = Seq[Int]()
      var currentContig = 0
      timeline.sliding(2).dropWhile(_.size < 2).foreach { pair =>
        val d1 = pair.head
        val d2 = pair.drop(1).head
        val hole = Duration.between(d1, d2).toDays.toInt - 1
        if (hole > 0) {
          contigSizes = contigSizes :+ (currentContig + 1)
          currentContig = 0
          holeSizes = holeSizes :+ hole
        } else {
          currentContig = currentContig + 1
        }
      }
      holeSizes = holeSizes.sorted
      contigSizes = contigSizes.sorted
      val meanContig = if (contigSizes.size > 2) {
        1.0 * contigSizes.drop(1).dropRight(1).sum / (contigSizes.size - 2)
      } else if (contigSizes.size > 0) {
        1.0 * contigSizes.sum / contigSizes.size
      } else {
        0.0
      }
      val meanHole = if (holeSizes.size > 2) {
        1.0 * holeSizes.drop(1).dropRight(1).sum / (holeSizes.size - 2)
      } else if (holeSizes.size > 0) {
        1.0 * holeSizes.sum / holeSizes.size
      } else {
        0.0
      }
      val syncSummary = SyncEndOfDayBySymbol(
        "BOND",
        symbol,
        dateOfCall = LocalDate.now().format(DateTimeFormatter.ISO_LOCAL_DATE),
        deltaChanges = diff.changed.size,
        deltaDatesAdded = diff.added.size,
        deltaDatesMissing = diff.removed.size,
        numDates = apiBondEods.size,
        minDate = apiBondEods.headOption.map(_.date).orNull,
        maxDate = apiBondEods.lastOption.map(_.date).orNull,
        hullSize = if (timeline.isEmpty) 0 else Duration.between(timeline.head, timeline.last).toDays.toInt,
        biggestContiguous = if (contigSizes.isEmpty) 0 else contigSizes.last,
        meanContiguous = meanContig,
        numHoles = holeSizes.size,
        biggestHole = if (holeSizes.isEmpty) 0 else holeSizes.last,
        meanHole = meanHole)

      for ((dbEodBond, apiEodBond) <- diff.changed) { // (in db) and (in api) and (different)
        logger.info(s"changed for (${dbEodBond.exchangeId}, ${dbEodBond.symbol}, ${dbEodBond.date}): ${dbEodBond.diff(apiEodBond)}")
        apiEodBond.insert(dbClient, upsert = true)
      }
      for (bondEod <- diff.removed) { // (in db) and (not in api)
        logger.info(s"bond end of day removed: $bondEod")
      }
      for (bondEod <- diff.added) { // (not in db) and (in api)
        bondEod.insert(dbClient)
      }
      for (bondEod <- diff.same) { // (in db) and (in api)
        // unchanged...
      }
      syncSummary.upsert(dbClient)
    }
  }

  def syncEodsByDate(exchangeId: String, date: String): Unit = {
    var apiTime = 0.0
    val apiEods = time(s => apiTime = s) {
      eodHdClient.eodByDate(exchangeId, date).get()
    }
    time { s =>
      logger.info(s"SyncClient.syncEodsByDate($exchangeId, $date) took $apiTime s retrieving from api, then took $s s")
    } {
      val dbEods = dbClient.eodByDate(exchangeId, date)

      val diff = Diffable.diff[(String, String, String), EodHdEndOfDay](dbEods, apiEods, eod => eod.key())
      logger.info(s"eod sync summary for ($exchangeId, $date): ${dbEods.size} from db, ${apiEods.size} from api, ${diff.changed.size} changed, ${diff.removed.size} removed, ${diff.added.size} added, ${diff.same.size} same")

      val syncSummary = SyncEndOfDayByDate(
        exchangeId,
        date,
        dateOfCall = LocalDate.now().format(DateTimeFormatter.ISO_LOCAL_DATE),
        deltaChanges = diff.changed.size,
        deltaSymbolsAdded = diff.added.size,
        deltaSymbolsMissing = diff.removed.size,
        numSymbols = apiEods.size)

      for ((dbEod, apiEod) <- diff.changed) { // (in db) and (in api) and (different)
        logger.info(s"changed for (${dbEod.exchangeId}, ${dbEod.symbol}, ${dbEod.date}): ${dbEod.diff(apiEod)}")
        apiEod.insert(dbClient, upsert = true)
      }
      for (eod <- diff.removed) { // (in db) and (not in api)
        logger.info(s"end of day removed: $eod")
      }
      for (eod <- diff.added) { // (not in db) and (in api)
        eod.insert(dbClient)
      }
      for (eod <- diff.same) { // (in db) and (in api)
        // unchanged...
      }
      syncSummary.upsert(dbClient)
    }
  }

  def syncEodsByDate(exchangeId: String, date: LocalDate): Unit = {
    syncEodsByDate(exchangeId, date.format(DateTimeFormatter.ISO_LOCAL_DATE))
  }


  def syncEodsByDate(): Unit = time { ms =>
    logger.info(s"SyncClient.syncEodsByDate() took $ms ms")
  } {
    // do we have everything except the last day?
    // if so, just grab the last day for 100 api calls
    // if not, then cycle through least recently synced first (so never synced would be absolute first)

    val exchanges = dbClient.exchanges()
    // minus 2 days is necessary to avoid the difference between 01-01T23:59 and 01-02T-1:01 (2 mins, but looks like 1 day)
    val yesterday = LocalDate.now().minusDays(2)
    val exchangeDateCallables: Seq[Callable[Try[Boolean]]] = exchanges.flatMap { exchange =>
      val oldestToCover: LocalDate = dbClient.eodByDateSyncing(exchange.exchangeId)
        .sortBy(_.date)
        .lastOption
        .map(es => LocalDate.parse(es.date, DateTimeFormatter.ISO_LOCAL_DATE))
        .filter(_.isBefore(yesterday))
        .getOrElse(yesterday)
      val newestToCover: LocalDate = yesterday
      // at least get the day before yesterday and yesterday
      // find most recent date, redo that date and the all dates up to and including yesterday
      var coverDay: LocalDate = oldestToCover
      var daysToCover = Set[LocalDate]()
      while (!coverDay.isAfter(newestToCover)) {
        daysToCover = daysToCover + coverDay
        coverDay = coverDay.plusDays(1)
      }
      // get day from 8 days ago (give them a week to make corrections...?)
      daysToCover = daysToCover + yesterday.minusDays(7)
      daysToCover.toSeq.map { date =>
        toCallable(
          //          logExceptions(
          syncEodsByDate(exchange.exchangeId, date)
          //          )
        )
      }
    }
    logger.info(s"invoking ${exchangeDateCallables.size} sync eod by dates")
    invokeAll(exchangeDateCallables)
  }

  // excludes BONDs
  def syncEodsBySymbol(unsyncedForDays: Int): Unit = time { ms =>
    logger.info(s"SyncClient.syncEodsBySymbol() took $ms ms")
  } {
    val exchanges = dbClient.exchanges()

    val syncedLatelyStr = LocalDate.now().minusDays(unsyncedForDays).format(DateTimeFormatter.ISO_LOCAL_DATE)
    val exchangeSymbolCallables: Seq[Callable[Try[Boolean]]] = exchanges
      .filter(_.exchangeId != "BOND")
      .flatMap { exchange =>
        val symbols: Set[String] = dbClient.symbols(exchange.exchangeId)
          .map(_.symbol).toSet
        val syncedSymbols: Set[String] = dbClient.eodBySymbolSyncing(exchange.exchangeId)
          .filter(_.dateOfCall >= syncedLatelyStr)
          .map(_.symbol).toSet
        val unsyncedSymbols: Set[String] = symbols -- syncedSymbols
        unsyncedSymbols.toSeq.map { symbol =>
          toCallable(
            //            logExceptions(
            syncEodsBySymbol(exchange.exchangeId, symbol)
            //            )
          )
        }
      }
    logger.info(s"invoking ${exchangeSymbolCallables.size} sync eod by symbols")
    invokeAll(exchangeSymbolCallables)
  }

  def syncBondEodsBySymbol(unsyncedForDays: Int): Unit = time { ms =>
    logger.info(s"SyncClient.syncBondEodsBySymbol() took $ms ms")
  } {
    val syncedLatelyStr = LocalDate.now().minusDays(unsyncedForDays).format(DateTimeFormatter.ISO_LOCAL_DATE)
    val symbols: Set[String] = dbClient.symbols("BOND")
      .map(_.symbol).toSet
    val syncedSymbols: Set[String] = dbClient.eodBySymbolSyncing("BOND")
      .filter(_.dateOfCall >= syncedLatelyStr)
      .map(_.symbol).toSet
    val unsyncedSymbols: Set[String] = symbols -- syncedSymbols

    val exchangeSymbolCallables: Seq[Callable[Try[Boolean]]] =
      unsyncedSymbols.toSeq.map { symbol =>
        toCallable(
          logExceptions(
            syncBondEodsBySymbol(symbol)
          )
        )
      }

    logger.info(s"invoking ${exchangeSymbolCallables.size} sync bond eod by symbols")
    invokeAll(exchangeSymbolCallables)
  }




  private def toCallable(runnable: => Unit) = new Callable[Try[Boolean]]() {
    override def call(): Try[Boolean] = Try {
      runnable
      true
    }
  }

  private def invokeAll(callables: Seq[Callable[Try[Boolean]]]): Unit = {
    val futures = executorService.invokeAll((callables.asJava))
    val numFailures = futures.asScala.map(_.get).filter(_.isFailure).size
    logger.debug(s"invoked ${callables.size} callables, got $numFailures failures")
  }




  def syncShares(exchangeId: String, symbol: String): Unit = {
    var apiTime = 0.0
    val apiShares: Seq[EodHdOutstandingShares] = time(s => apiTime = s) {
      eodHdClient.outstandingShares(exchangeId, symbol).get()
    }
    time { s =>
      logger.info(s"SyncClient.syncShares($exchangeId, $symbol) took $apiTime s retrieving from api, then took $s s")
    } {
      val dbShares: Seq[EodHdOutstandingShares] = dbClient.shares(exchangeId, symbol)

      val diff = Diffable.diff[(String, String, String), EodHdOutstandingShares](dbShares, apiShares, shares => shares.key())
      logger.info(s"shares sync summary for ($exchangeId, $symbol): ${dbShares.size} from db, ${apiShares.size} from api, ${diff.changed.size} changed, ${diff.removed.size} removed, ${diff.added.size} added, ${diff.same.size} same")


      for ((dbShares, apiShares) <- diff.changed) { // (in db) and (in api) and (different)
        logger.info(s"changed for (${dbShares.exchangeId}, ${dbShares.symbol}, ${dbShares.date}): ${dbShares.diff(apiShares)}")
        dbClient.insertShares(apiShares, upsert = true)
      }
      for (shares <- diff.removed) { // (in db) and (not in api)
        logger.info(s"shares removed: $shares")
      }
      for (shares <- diff.added) { // (not in db) and (in api)
        dbClient.insertShares(shares)
      }
      for (shares <- diff.same) { // (in db) and (in api)
        // unchanged...
      }
    }
  }

}
