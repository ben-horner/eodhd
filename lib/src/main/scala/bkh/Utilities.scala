package bkh

import org.apache.log4j.Logger

object Utilities {
  val logger = Logger.getLogger("Utilities")

  def time[T](recordSeconds: Double => Unit)(f: => T): T = {
    val start = System.nanoTime()
    val result: T = f
    val duration = (System.nanoTime() - start) / 1000000000.0
    recordSeconds(duration)
    result
  }

  def logExceptions[T](f: => T): T = {
    try {
      f
    } catch {
      case  e: Exception =>
        logger.error("caught exception", e)
        throw e
    }
  }

}
