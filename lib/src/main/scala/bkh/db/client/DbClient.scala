package bkh.db.client

import java.sql._
import java.time.format.DateTimeFormatter
import java.time.{Duration, LocalDate}

import bkh.Utilities._
import bkh.api.entities._
import org.apache.log4j.Logger

case class DbClient(user: String) {
  import DbClient._

  private var allConnections = Seq[Connection]()
  private var allStatements = Seq[Statement]()

  val connection = new ThreadLocal[Connection](){
    override def initialValue(): Connection = {
      val result = DriverManager.getConnection(s"$jdbcConnectStr?user=$user")
      result.setAutoCommit(false) // must call connection.commit() (or connection.rollback())...
      allConnections = allConnections :+ result
      result
    }
  }

  val statement = new ThreadLocal[Statement](){
    override def initialValue(): Statement = {
      val result = connection.get().createStatement()
      allStatements = allStatements :+ result
      result
    }
  }

  def close(): Unit = {
    allStatements.foreach(_.close())
    allConnections.foreach(_.close())
  }

  
  

  // query the entire cache of exchanges (~74)
  def exchanges(): Seq[EodHdExchange] = time { s =>
    logger.info(s"DbClient.exchanges() took $s s")
  }{
    val query = s"select * from eodhd_exchange"
    drainResultSet(query, EodHdExchange.fromResultSet)
  }

  // metadata about the lists deltas
  def exchangeListSyncing(): Seq[SyncExchangeList] = {
    val query = s"select * from sync_exchange_list"
    drainResultSet(query, SyncExchangeList.fromResultSet)
  }

  
  
  
  def symbols(exchangeId: String): Seq[EodHdSymbol] = time { s =>
    logger.info(s"DbClient.symbols($exchangeId) took $s s")
  }{
    val query = s"select * from eodhd_symbol where exchange_id = '$exchangeId'"
    drainResultSet(query, EodHdSymbol.fromResultSet)
  }

  // metadata about the lists deltas
  def symbolListSyncing(exchangeId: String): Seq[SyncSymbolList] = {
    val query = s"select * from sync_symbol_list where exchange_id = '$exchangeId'"
    drainResultSet(query, SyncSymbolList.fromResultSet)
  }




  def eodBySymbol(exchangeId: String, symbol: String): Seq[EodHdEndOfDay] = time { s =>
    logger.info(s"DbClient.eodSymbol($exchangeId, $symbol) took $s s")
  }{
    val query = s"select * from eodhd_end_of_day where exchange_id = '$exchangeId' and symbol = '$symbol'"
    drainResultSet(query, EodHdEndOfDay.fromResultSet)
  }

  def bondEodBySymbol(symbol: String): Seq[EodHdBondEndOfDay] = time { s =>
    logger.info(s"DbClient.eodBondBySymbol($symbol) took $s s")
  }{
    val query = s"select * from eodhd_bond_end_of_day where exchange_id = 'BOND' and symbol = '$symbol'"
    drainResultSet(query, EodHdBondEndOfDay.fromResultSet)
  }

  def eodBySymbolSyncing(exchangeId: String, symbol: String): Option[SyncEndOfDayBySymbol] = time { s =>
    logger.info(s"DbClient.eodSymbolSyncing($exchangeId, $symbol) took $s s")
  }{
    val query = s"select * from sync_end_of_day_by_symbol where exchange_id = '$exchangeId' and symbol = '$symbol'"
    drainResultSet(query, SyncEndOfDayBySymbol.fromResultSet).headOption
  }

  def eodBySymbolSyncing(exchangeId: String): Seq[SyncEndOfDayBySymbol] = time { s =>
    logger.info(s"DbClient.eodBySymbolSyncing($exchangeId) took $s s")
  }{
    val query = s"select * from sync_end_of_day_by_symbol where exchange_id = '$exchangeId'"
    drainResultSet(query, SyncEndOfDayBySymbol.fromResultSet)
  }

  def eodByDate(exchangeId: String, date: String): Seq[EodHdEndOfDay] = time { s =>
    logger.info(s"DbClient.eodDate($exchangeId, $date) took $s s")
  }{
    val query = s"select * from eodhd_end_of_day where exchange_id = '$exchangeId' and date = '$date'"
    drainResultSet(query, EodHdEndOfDay.fromResultSet)
  }

  def eodByDate(exchangeId: String, date: LocalDate): Seq[EodHdEndOfDay] = {
    eodByDate(exchangeId, date.format(DateTimeFormatter.ISO_LOCAL_DATE))
  }

  def eodByDateSyncing(exchangeId: String, date: String): Option[SyncEndOfDayByDate] = time { s =>
    logger.info(s"DbClient.eodDateSyncing($exchangeId, $date) took $s s")
  }{
    val query = s"select * from sync_end_of_day_by_date where exchange_id = '$exchangeId' and symbol = '$date'"
    drainResultSet(query, SyncEndOfDayByDate.fromResultSet).headOption
  }

  def eodByDateSyncing(exchangeId: String, date: LocalDate): Option[SyncEndOfDayByDate] = {
    eodByDateSyncing(exchangeId, date.format(DateTimeFormatter.ISO_LOCAL_DATE))
  }

  def eodByDateSyncing(exchangeId: String): Seq[SyncEndOfDayByDate] = time { s =>
    logger.info(s"DbClient.eodByDateSyncing($exchangeId) took $s s")
  }{
    val query = s"select * from sync_end_of_day_by_date where exchange_id = '$exchangeId'"
    drainResultSet(query, SyncEndOfDayByDate.fromResultSet)
  }




  def shares(exchangeId: String, symbol: String): Seq[EodHdOutstandingShares] = time { s =>
    logger.info(s"DbClient.shares($exchangeId, $symbol) took $s s")
  }{
    val query = s"select * from eodhd_shares where exchange_id = '$exchangeId' and symbol = '$symbol'"
    drainResultSet(query, shares)
  }











  def insertShares(shares: EodHdOutstandingShares, upsert: Boolean = false): Unit = {
    val sharesUpdate =
      s"""insert into eodhd_shares
         |(exchange_id, symbol, date, shares)
         |values
         |(  ${nullableString(shares.exchangeId)},
         |   ${nullableString(shares.symbol)},
         |   ${nullableString(shares.date)},
         |   ${shares.shares}
         |)
         |""".stripMargin
    val update =
      if (upsert) sharesUpdate + sharesUpsertSuffix
      else sharesUpdate
    try{
      val statement = this.statement.get()
      statement.executeUpdate(update)
      statement.getConnection.commit()
    } catch {
      case e: Exception =>
        logger.error(s"failed to insert shares: $shares", e)
        throw new RuntimeException(s"failed to insert shares: $shares", e)
    }
  }

  private val sharesUpsertSuffix =
    s"""on conflict on constraint eodhd_shares_exchange_id_symbol_date_pkey
       |do update set
       |  shares = excluded.shares
       |""".stripMargin




  private def drainResultSet[T](query: String, transform: ResultSet => T): Seq[T] = {
    val statement = this.statement.get()
    val rs: ResultSet = statement.executeQuery(query)
    var result = Seq[T]()
    while(rs.next()) {
      result = result :+ transform(rs)
    }
    rs.close()
    result
  }



  private def shares(rs: ResultSet): EodHdOutstandingShares = {
    EodHdOutstandingShares(
      rs.getString    ("exchange_id"),
      rs.getString    ("symbol"     ),
      rs.getString    ("date"       ),
      rs.getBigDecimal("shares"     )
    ).scrub()
  }

}

object DbClient {
  private val jdbcConnectStr: String = "jdbc:postgresql://localhost/eodhd"

  def nullableString(value: String): String = {
    Option(value).map(v => s"'$v'").orNull
  }

  private val logger = Logger.getLogger("DbClient")

}
