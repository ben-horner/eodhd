package bkh.api.entities

import java.sql.ResultSet

import bkh.db.client.DbClient
import bkh.db.client.DbClient._
import com.fasterxml.jackson.annotation.JsonProperty
import org.apache.log4j.Logger

case class SyncExchangeList(@JsonProperty("exchange_id") exchangeId: String,
                            @JsonProperty("first_seen") firstSeen: String,
                            @JsonProperty("last_updated") lastUpdated: String,
                            @JsonProperty("last_seen") lastSeen: String,
                            @JsonProperty("last_absent") lastAbsent: String,
                            @JsonProperty("last_reappeared") lastReappeared: String){

  def key(): String = exchangeId

  def upsert(dbClient: DbClient): Unit = {
    import SyncExchangeList._
    val syncExchangeListUpdate =
      s"""insert into sync_exchange_list
         |(exchange_id, first_seen, last_updated, last_seen, last_absent, last_reappeared)
         |values
         |(  ${nullableString(exchangeId)} ,
         |   ${nullableString(firstSeen)}  ,
         |   ${nullableString(lastUpdated)},
         |   ${nullableString(lastSeen)}   ,
         |   ${nullableString(lastAbsent)} ,
         |   ${nullableString(lastReappeared)}
         |)
         |on conflict on constraint sync_exchange_list_exchange_id_pkey
         |do update set
         |  first_seen      = excluded.first_seen  ,
         |  last_updated    = excluded.last_updated,
         |  last_seen       = excluded.last_seen   ,
         |  last_absent     = excluded.last_absent ,
         |  last_reappeared = excluded.last_reappeared
         |""".stripMargin
    val statement = dbClient.statement.get()
    try {
      statement.executeUpdate(syncExchangeListUpdate)
      statement.getConnection.commit()
    } catch {
      case t: Throwable =>
        val errorMsg = s"failed to insert into sync_exchange_list (key=${key()}):\n$syncExchangeListUpdate"
        logger.error(errorMsg, t)
        statement.getConnection.rollback()
        throw new RuntimeException(errorMsg, t)
    }
  }

}

object SyncExchangeList {

  val logger: Logger = Logger.getLogger("EodHdExchangeListSync")

  def fromResultSet(rs: ResultSet): SyncExchangeList = {
    SyncExchangeList(
      rs.getString("exchange_id"    ),
      rs.getString("first_seen"     ),
      rs.getString("last_updated"   ),
      rs.getString("last_seen"      ),
      rs.getString("last_absent"    ),
      rs.getString("last_reappeared")
    )
  }
}
