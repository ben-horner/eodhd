package bkh.api.entities

import bkh.api.entities.Diffable.{BigDecimalDiff, StringDiff, bigDecimalDiff, stringDiff}
import bkh.api.entities.Scrubbing.nullableBigDecimal
import com.fasterxml.jackson.annotation.JsonProperty

case class EodHdOutstandingShares(exchangeId: String,
                                  symbol: String,
                                  @JsonProperty("dateFormatted") date: String,
                                  @JsonProperty("shares") shares: BigDecimal)
  extends Diffable[EodHdOutstandingSharesDiff, EodHdOutstandingShares]{
  def key(): (String, String, String) = (exchangeId, symbol, date)
  def scrub(): EodHdOutstandingShares = {
    this.copy(shares = nullableBigDecimal(shares, 4))
  }
  override def diff(that: EodHdOutstandingShares): EodHdOutstandingSharesDiff = EodHdOutstandingSharesDiff(
    stringDiff(this.exchangeId, that.exchangeId),
    stringDiff(this.symbol, that.symbol),
    stringDiff(this.date, that.date),
    bigDecimalDiff(this.shares, that.shares))
}

case class EodHdOutstandingSharesDiff(exchangeId: StringDiff,
                                      symbol: StringDiff,
                                      date: StringDiff,
                                      shares: BigDecimalDiff)
