package bkh.api.entities

import java.sql.{ResultSet, Statement}

import bkh.api.entities.Diffable.{StringDiff, stringDiff}
import bkh.db.client.DbClient
import bkh.db.client.DbClient._
import com.fasterxml.jackson.annotation.JsonProperty
import org.apache.log4j.Logger

case class EodHdExchange(@JsonProperty("Code") exchangeId: String,
                         @JsonProperty("Name") name: String,
                         @JsonProperty("Country") country: String,
                         @JsonProperty("Currency") currency: String,
                         @JsonProperty("OperatingMIC") operatingMic: String)
  extends Diffable[EodHdExchangeDiff, EodHdExchange]{

  def key(): String = exchangeId

  def scrub(): EodHdExchange = {
    this.copy(name = name.replaceAll("&amp;", "&"))
  }

  override def diff(that: EodHdExchange): EodHdExchangeDiff = EodHdExchangeDiff(
    stringDiff(this.exchangeId, that.exchangeId),
    stringDiff(this.name, that.name),
    stringDiff(this.country, that.country),
    stringDiff(this.currency, that.currency),
    stringDiff(this.operatingMic, that.operatingMic))

  def insert(dbClient: DbClient, upsert: Boolean = false): Unit = {
    import EodHdExchange._
    val exchangeUpdate =
      s"""insert into eodhd_exchange
         |(exchange_id, name, country, currency, operating_mic)
         |values
         |(  ${nullableString(exchangeId)},
         |   ${nullableString(name)},
         |   ${nullableString(country)},
         |   ${nullableString(currency)},
         |   ${nullableString(operatingMic)}
         |)
         |""".stripMargin
    val update =
      if (upsert) exchangeUpdate + exchangeUpsertSuffix
      else exchangeUpdate
    val statement = dbClient.statement.get()
    try {
      statement.executeUpdate(update)
      statement.getConnection.commit()
    } catch {
      case t: Throwable =>
        val errorMsg = s"failed to insert into eodhd_exchange (key=${key()}:\n$update"
        logger.error(errorMsg, t)
        statement.getConnection.rollback()
        throw new RuntimeException(errorMsg, t)
    }
  }

}

object EodHdExchange {

  val logger: Logger = Logger.getLogger("EodHdExchange")

  private val exchangeUpsertSuffix =
    s"""on conflict on constraint eodhd_exchange_exchange_id_pkey
       |do update set
       |  name          = excluded.name    ,
       |  country       = excluded.country ,
       |  currency      = excluded.currency,
       |  operating_mic = excluded.operating_mic
       |""".stripMargin

  def fromResultSet(rs: ResultSet): EodHdExchange = {
    EodHdExchange(
      rs.getString("exchange_id"  ),
      rs.getString("name"         ),
      rs.getString("country"      ),
      rs.getString("currency"     ),
      rs.getString("operating_mic")
    ).scrub()
  }

}

case class EodHdExchangeDiff(exchangeId: StringDiff,
                             name: StringDiff,
                             country: StringDiff,
                             currency: StringDiff,
                             operatingMic: StringDiff)
