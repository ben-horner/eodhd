package bkh.api.entities

import java.sql.ResultSet

import bkh.api.entities.Diffable.{BigDecimalDiff, StringDiff, bigDecimalDiff, stringDiff}
import bkh.api.entities.Scrubbing.nullableBigDecimal
import bkh.db.client.DbClient
import bkh.db.client.DbClient._
import com.fasterxml.jackson.annotation.JsonProperty
import org.apache.log4j.Logger

case class EodHdEndOfDay(@JsonProperty("exchange_short_name") exchangeId: String,
                         @JsonProperty("code") symbol: String,
                         @JsonProperty("date") date: String,
                         @JsonProperty("open") open: BigDecimal,
                         @JsonProperty("high") high: BigDecimal,
                         @JsonProperty("low") low: BigDecimal,
                         @JsonProperty("close") close: BigDecimal,
                         @JsonProperty("adjusted_close") adjustedClose: BigDecimal,
                         @JsonProperty("volume") volume: BigDecimal)
  extends Diffable[EodHdEndOfDayDiff, EodHdEndOfDay] {

  def key(): (String, String, String) = (exchangeId, symbol, date)

  def scrub(): EodHdEndOfDay = {
    // need to truncate numbers to 4 decimal places (subject to change) to prevent minuscule differences looking like errors
    this.copy(
      open = nullableBigDecimal(open, 4),
      high = nullableBigDecimal(high, 4),
      low = nullableBigDecimal(low, 4),
      close = nullableBigDecimal(close, 4),
      adjustedClose = nullableBigDecimal(adjustedClose, 4),
      volume = nullableBigDecimal(volume, 4)
    )
  }

  override def diff(that: EodHdEndOfDay): EodHdEndOfDayDiff = EodHdEndOfDayDiff(
    stringDiff(this.exchangeId, that.exchangeId),
    stringDiff(this.symbol, that.symbol),
    stringDiff(this.date, that.date),
    bigDecimalDiff(this.open, that.open),
    bigDecimalDiff(this.high, that.high),
    bigDecimalDiff(this.low, that.low),
    bigDecimalDiff(this.close, that.close),
    bigDecimalDiff(this.adjustedClose, that.adjustedClose),
    bigDecimalDiff(this.volume, that.volume))

  def insert(dbClient: DbClient, upsert: Boolean = false): Unit = {
    import EodHdEndOfDay._
    val eodUpdate =
      s"""insert into eodhd_end_of_day
         |(exchange_id, symbol, date, open, high, low, close, adjusted_close, volume)
         |values
         |(  ${nullableString(exchangeId)},
         |   ${nullableString(symbol)},
         |   ${nullableString(date)},
         |   ${open},
         |   ${high},
         |   ${low},
         |   ${close},
         |   ${adjustedClose},
         |   ${volume}
         |)
         |""".stripMargin
    val update =
      if (upsert) eodUpdate + eodUpsertSuffix
      else eodUpdate
    val statement = dbClient.statement.get()
    try {
      statement.executeUpdate(update)
      statement.getConnection.commit()
    } catch {
      case e: Exception =>
        val errorMsg = s"failed to insert into eodhd_end_of_day (key=${key()}):\n$update"
        logger.error(errorMsg, e)
        statement.getConnection.rollback()
        throw new RuntimeException(errorMsg, e)
    }
  }

}

object EodHdEndOfDay {

  val logger: Logger = Logger.getLogger("EodHdEndOfDay")

  val eodUpsertSuffix =
    s"""on conflict on constraint eodhd_end_of_day_exchange_id_symbol_date_pkey
       |do update set
       |  open           = excluded.open          ,
       |  high           = excluded.high          ,
       |  low            = excluded.low           ,
       |  close          = excluded.close         ,
       |  adjusted_close = excluded.adjusted_close,
       |  volume         = excluded.volume
       |""".stripMargin

  def fromResultSet(rs: ResultSet): EodHdEndOfDay = {
    EodHdEndOfDay(
      rs.getString    ("exchange_id"   ),
      rs.getString    ("symbol"        ),
      rs.getString    ("date"          ),
      rs.getBigDecimal("open"          ),
      rs.getBigDecimal("high"          ),
      rs.getBigDecimal("low"           ),
      rs.getBigDecimal("close"         ),
      rs.getBigDecimal("adjusted_close"),
      rs.getBigDecimal("volume"        )
    ).scrub()
  }

}

case class EodHdEndOfDayDiff(exchangeId: StringDiff,
                             symbol: StringDiff,
                             date: StringDiff,
                             open: BigDecimalDiff,
                             high: BigDecimalDiff,
                             low: BigDecimalDiff,
                             close: BigDecimalDiff,
                             adjustedClose: BigDecimalDiff,
                             volume: BigDecimalDiff)
