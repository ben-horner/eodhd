package bkh.api.entities

trait Diffable[D, T <: Diffable[D, T]] {
  def diff(that: T): D
}

object Diffable {

  def diff[K, T](prevState: Seq[T], currentState: Seq[T], keyBy: T => K): Diff[T] = {
    val prevMap: Map[K, T] = oneToOneKeyBy(prevState, keyBy)
    val currentMap: Map[K, T] = oneToOneKeyBy(currentState, keyBy)
    val keys = prevMap.keySet ++ currentMap.keySet
    var changed = Seq[(T, T)]()
    var removed = Seq[T]()
    var added = Seq[T]()
    var same = Seq[T]()
    for (key <- keys) {
      if (currentMap.contains(key) && !prevMap.contains(key)) {
        added = added :+ currentMap(key)
      } else if (!currentMap.contains(key) && prevMap.contains(key)) {
        removed = removed :+ prevMap(key)
      } else if (currentMap(key) != prevMap(key)) {
        changed = changed :+ (prevMap(key), currentMap(key))
      } else {
        same = same :+ prevMap(key)
      }
    }
    Diff[T](changed, removed, added, same)
  }

  type StringDiff = Option[(String, String)]
  def stringDiff(s1: String, s2: String): StringDiff = {
    if (s1 == s2) None
    else Some((s1, s2))
  }

  type IntDiff = Option[(Int, Int)]
  def intDiff(i1: Int, i2: Int): IntDiff = {
    if (i1 == i2) None
//    else Some(math.abs(i1 - i2))
    else Some((i1, i2))
  }

  type DoubleDiff = Option[(Double, Double)]
  def doubleDiff(d1: Double, d2: Double): DoubleDiff = {
    if (d1 == d2) None
//    else Some(math.abs(d1 - d2))
    else Some((d1, d2))
  }

  type BigDecimalDiff = Option[(BigDecimal, BigDecimal)]
  def bigDecimalDiff(bd1: BigDecimal, bd2: BigDecimal): BigDecimalDiff = {
    if (bd1 == bd2) None
//    else Some((bd1 - bd2).abs)
    else Some((bd1, bd2))
  }

  def oneToOneKeyBy[K, T](s: Seq[T], keyBy: T => K): Map[K, T] = {
    val grouped = s.groupBy(keyBy)
    if (grouped.nonEmpty) {
      val (worstKey, mapped) = grouped.toSeq.maxBy(_._2.size)
      if (mapped.size > 1)
        throw new IllegalArgumentException(s"key function did not provide one to one mapping, worst key: $worstKey -> $mapped")
    }
    grouped.map{ case (k, vs) => k -> vs.head }
  }

}