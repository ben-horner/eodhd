package bkh.api.entities

import java.sql.ResultSet

import bkh.api.entities.Diffable.{StringDiff, stringDiff}
import bkh.db.client.DbClient
import bkh.db.client.DbClient._
import com.fasterxml.jackson.annotation.JsonProperty
import org.apache.log4j.Logger

case class EodHdSymbol(exchangeId: String,
                       @JsonProperty("Exchange") exchange: String,
                       @JsonProperty("Code") symbol: String,
                       @JsonProperty("Name") name: String,
                       @JsonProperty("Country") country: String,
                       @JsonProperty("Currency") currency: String,
                       @JsonProperty("Type") symbolType: String)
  extends Diffable[EodHdSymbolDiff, EodHdSymbol] {

  def key(): (String, String) = (exchangeId, symbol)

  def scrub(): EodHdSymbol = {
    // stripping single quotes, not sure how to escape them into postgres correctly
    this.copy(
      name = Option(name).map(_
        .replaceAll("&amp;", "&")
        .replaceAll("'", "")
      ).orNull
    )

  }

  override def diff(that: EodHdSymbol): EodHdSymbolDiff = EodHdSymbolDiff(
    stringDiff(this.exchangeId, that.exchangeId),
    stringDiff(this.exchange, that.exchange),
    stringDiff(this.symbol, that.symbol),
    stringDiff(this.name, that.name),
    stringDiff(this.country, that.country),
    stringDiff(this.currency, that.currency),
    stringDiff(this.symbolType, that.symbolType))

  def insert(dbClient: DbClient, upsert: Boolean = false): Unit = {
    import EodHdSymbol._
    val symbolUpdate =
      s"""insert into eodhd_symbol
         |(exchange_id, exchange, symbol, name, country, currency, type)
         |values
         |(  ${nullableString(exchangeId)},
         |   ${nullableString(exchange)},
         |   ${nullableString(symbol)},
         |   ${nullableString(name)},
         |   ${nullableString(country)},
         |   ${nullableString(currency)},
         |   ${nullableString(symbolType)}
         |)
         |""".stripMargin
    val update =
      if (upsert) symbolUpdate + symbolUpsertSuffix
      else symbolUpdate
    val statement = dbClient.statement.get()
    try {
      statement.executeUpdate(update)
      statement.getConnection.commit()
    } catch {
      case t: Throwable =>
        val errorMsg = s"failed to insert into eodhd_symbol (key=${key()}):\n$update"
        logger.error(errorMsg, t)
        statement.getConnection.rollback()
        throw new RuntimeException(errorMsg, t)
    }
  }

}

object EodHdSymbol {

  val logger: Logger = Logger.getLogger("EodHdSymbol")

  val symbolUpsertSuffix =
    s"""on conflict on constraint eodhd_symbol_exchange_id_symbol_pkey
       |do update set
       |  exchange = excluded.exchange,
       |  name     = excluded.name    ,
       |  country  = excluded.country ,
       |  currency = excluded.currency,
       |  type     = excluded.type
       |""".stripMargin

  def fromResultSet(rs: ResultSet): EodHdSymbol = {
    EodHdSymbol(
      rs.getString("exchange_id"),
      rs.getString("exchange"   ),
      rs.getString("symbol"     ),
      rs.getString("name"       ),
      rs.getString("country"    ),
      rs.getString("currency"   ),
      rs.getString("type"       )
    ).scrub()
  }

}

case class EodHdSymbolDiff(exchangeId: StringDiff,
                           exchange: StringDiff,
                           symbol: StringDiff,
                           name: StringDiff,
                           country: StringDiff,
                           currency: StringDiff,
                           symbolType: StringDiff)
