package bkh.api.entities

import java.sql.ResultSet

import bkh.db.client.DbClient
import bkh.db.client.DbClient._
import org.apache.log4j.Logger

case class SyncEndOfDayByDate(exchangeId: String,
                              date: String,
                              dateOfCall: String,
                              deltaChanges: Int,
                              deltaSymbolsAdded: Int,
                              deltaSymbolsMissing: Int,
                              numSymbols: Int) {

  def key(): (String, String) = (exchangeId, date)

  def upsert(dbClient: DbClient): Unit = {
    import SyncEndOfDayByDate._
    val syncEndOfDayByDateUpdate =
      s"""insert into sync_end_of_day_by_date
         |(exchange_id, date, date_of_call, delta_changes, delta_symbols_added, delta_symbols_missing, num_symbols)
         |values
         |(  ${nullableString(exchangeId)},
         |   ${nullableString(date)},
         |   ${nullableString(dateOfCall)},
         |   ${deltaChanges},
         |   ${deltaSymbolsAdded},
         |   ${deltaSymbolsMissing},
         |   ${numSymbols}
         |)
         |on conflict on constraint sync_end_of_day_by_date_exchange_id_date_pkey
         |do update set
         |  date_of_call          = excluded.date_of_call         ,
         |  delta_changes         = excluded.delta_changes        ,
         |  delta_symbols_added   = excluded.delta_symbols_added  ,
         |  delta_symbols_missing = excluded.delta_symbols_missing,
         |  num_symbols           = excluded.num_symbols
         |""".stripMargin
    val statement = dbClient.statement.get()
    try {
      statement.executeUpdate(syncEndOfDayByDateUpdate)
      statement.getConnection.commit()
    } catch {
      case t: Throwable =>
        val errorMsg = s"failed to insert into sync_end_of_day_by_date (key=${key()}):\n$syncEndOfDayByDateUpdate"
        logger.error(errorMsg, t)
        statement.getConnection.rollback()
        throw new RuntimeException(errorMsg, t)
    }
  }

}


object SyncEndOfDayByDate {

  val logger: Logger = Logger.getLogger("SyncEndOfDayByDate")

  def fromResultSet(rs: ResultSet): SyncEndOfDayByDate = {
    SyncEndOfDayByDate(
      rs.getString("exchange_id"          ),
      rs.getString("date"                 ),
      rs.getString("date_of_call"         ),
      rs.getInt   ("delta_changes"        ),
      rs.getInt   ("delta_symbols_added"  ),
      rs.getInt   ("delta_symbols_missing"),
      rs.getInt   ("num_symbols"          )
    )
  }

}
