package bkh.api.entities

import java.sql.ResultSet

import bkh.db.client.DbClient
import bkh.db.client.DbClient.nullableString
import com.fasterxml.jackson.annotation.JsonProperty
import org.apache.log4j.Logger

case class SyncSymbolList(@JsonProperty("exchange_id") exchangeId: String,
                          @JsonProperty("symbol") symbol: String,
                          @JsonProperty("first_seen") firstSeen: String,
                          @JsonProperty("last_updated") lastUpdated: String,
                          @JsonProperty("last_seen") lastSeen: String,
                          @JsonProperty("last_absent") lastAbsent: String,
                          @JsonProperty("last_reappeared") lastReappeared: String){

  def key(): (String, String) = (exchangeId, symbol)

  def upsert(dbClient: DbClient): Unit = {
    import SyncSymbolList._
    val symbolListSyncUpdate =
      s"""insert into sync_symbol_list
         |(exchange_id, symbol, first_seen, last_updated, last_seen, last_absent, last_reappeared)
         |values
         |(  ${nullableString(exchangeId)} ,
         |   ${nullableString(symbol)}     ,
         |   ${nullableString(firstSeen)}  ,
         |   ${nullableString(lastUpdated)},
         |   ${nullableString(lastSeen)}   ,
         |   ${nullableString(lastAbsent)} ,
         |   ${nullableString(lastReappeared)}
         |)
         |on conflict on constraint sync_symbol_list_exchange_id_symbol_pkey
         |do update set
         |  first_seen      = excluded.first_seen  ,
         |  last_updated    = excluded.last_updated,
         |  last_seen       = excluded.last_seen   ,
         |  last_absent     = excluded.last_absent ,
         |  last_reappeared = excluded.last_reappeared
         |""".stripMargin
    val statement = dbClient.statement.get()
    try {
      statement.executeUpdate(symbolListSyncUpdate)
      statement.getConnection.commit()
    } catch {
      case t: Throwable =>
        val errorMsg = s"failed to insert into sync_symbol_list (key=${key()}):\n$symbolListSyncUpdate"
        logger.error(errorMsg, t)
        statement.getConnection.rollback()
        throw new RuntimeException(errorMsg, t)
    }

  }

}

object SyncSymbolList {

  val logger: Logger = Logger.getLogger("EodHdSymbolListSync")

  def fromResultSet(rs: ResultSet): SyncSymbolList = {
    SyncSymbolList(
      rs.getString("exchange_id"    ),
      rs.getString("symbol"         ),
      rs.getString("first_seen"     ),
      rs.getString("last_updated"   ),
      rs.getString("last_seen"      ),
      rs.getString("last_absent"    ),
      rs.getString("last_reappeared")
    )
  }

}
