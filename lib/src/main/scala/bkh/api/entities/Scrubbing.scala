package bkh.api.entities

import scala.math.BigDecimal.RoundingMode

object Scrubbing {

  def nullableBigDecimal(bigDecimal: BigDecimal, scale: Int): BigDecimal = {
    Option(bigDecimal).map(_.setScale(scale, RoundingMode.HALF_UP)).orNull
  }

}
