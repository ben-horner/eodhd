package bkh.api.entities

case class Diff[T](changed: Seq[(T, T)],
                   removed: Seq[T],
                   added: Seq[T],
                   same: Seq[T])
