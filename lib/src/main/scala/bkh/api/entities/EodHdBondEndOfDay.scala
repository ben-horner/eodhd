package bkh.api.entities

import java.sql.ResultSet

import bkh.api.entities.Diffable.{BigDecimalDiff, StringDiff, bigDecimalDiff, stringDiff}
import bkh.api.entities.Scrubbing.nullableBigDecimal
import bkh.db.client.DbClient
import bkh.db.client.DbClient._
import com.fasterxml.jackson.annotation.JsonProperty
import org.apache.log4j.Logger

case class EodHdBondEndOfDay(@JsonProperty("exchange_short_name") exchangeId: String,
                             @JsonProperty("code") symbol: String,
                             @JsonProperty("date") date: String,
                             @JsonProperty("price") price: BigDecimal,
                             @JsonProperty("yield") bondYield: BigDecimal,
                             @JsonProperty("volume") volume: BigDecimal)
  extends Diffable[EodHdBondEndOfDayDiff, EodHdBondEndOfDay] {

  def key(): (String, String, String) = (exchangeId, symbol, date)

  def scrub(): EodHdBondEndOfDay = {
    // need to truncate numbers to 4 decimal places (subject to change) to prevent minuscule differences looking like errors
    this.copy(
      price = nullableBigDecimal(price, 4),
      bondYield = nullableBigDecimal(bondYield, 4),
      volume = nullableBigDecimal(volume, 4)
    )
  }

  override def diff(that: EodHdBondEndOfDay): EodHdBondEndOfDayDiff = EodHdBondEndOfDayDiff(
    stringDiff(this.exchangeId, that.exchangeId),
    stringDiff(this.symbol, that.symbol),
    stringDiff(this.date, that.date),
    bigDecimalDiff(this.price, that.price),
    bigDecimalDiff(this.bondYield, that.bondYield),
    bigDecimalDiff(this.volume, that.volume))

  def insert(dbClient: DbClient, upsert: Boolean = false): Unit = {
    import EodHdBondEndOfDay._
    val eodUpdate =
      s"""insert into eodhd_bond_end_of_day
         |(exchange_id, symbol, date, price, yield, volume)
         |values
         |(  ${nullableString(exchangeId)},
         |   ${nullableString(symbol)},
         |   ${nullableString(date)},
         |   ${price},
         |   ${bondYield},
         |   ${volume}
         |)
         |""".stripMargin
    val update =
      if (upsert) eodUpdate + eodBondUpsertSuffix
      else eodUpdate
    val statement = dbClient.statement.get()
    try {
      statement.executeUpdate(update)
      statement.getConnection.commit()
    } catch {
      case e: Exception =>
        val errorMsg = s"failed to insert into eodhd_bond_end_of_day (key=${key()}):\n$update"
        logger.error(errorMsg, e)
        statement.getConnection.rollback()
        throw new RuntimeException(errorMsg, e)
    }
  }

}

object EodHdBondEndOfDay {

  val logger: Logger = Logger.getLogger("EodHdBondEndOfDay")

  val eodBondUpsertSuffix =
    s"""on conflict on constraint eodhd_bond_end_of_day_exchange_id_symbol_date_pkey
       |do update set
       |  price           = excluded.price,
       |  yield           = excluded.yield,
       |  volume         = excluded.volume
       |""".stripMargin

  def fromResultSet(rs: ResultSet): EodHdBondEndOfDay = {
    EodHdBondEndOfDay(
      rs.getString    ("exchange_id"),
      rs.getString    ("symbol"     ),
      rs.getString    ("date"       ),
      rs.getBigDecimal("price"      ),
      rs.getBigDecimal("yield"      ),
      rs.getBigDecimal("volume"     )
    ).scrub()
  }

}

case class EodHdBondEndOfDayDiff(exchangeId: StringDiff,
                                 symbol: StringDiff,
                                 date: StringDiff,
                                 price: BigDecimalDiff,
                                 bondYield: BigDecimalDiff,
                                 volume: BigDecimalDiff)
