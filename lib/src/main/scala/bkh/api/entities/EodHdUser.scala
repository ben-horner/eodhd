package bkh.api.entities

import com.fasterxml.jackson.annotation.JsonProperty

case class EodHdUser(@JsonProperty("name") name: String,
                     @JsonProperty("email") email: String,
                     @JsonProperty("subscriptionType") subscriptionType: String,
                     @JsonProperty("paymentMethod") paymentMethod: String,
                     @JsonProperty("apiRequests") apiRequests: Int,
                     @JsonProperty("apiRequestsDate") apiRequestsDate: String,
                     @JsonProperty("dailyRateLimit") dailyRateLimit: Int,
                     @JsonProperty("inviteToken") inviteToken: String,
                     @JsonProperty("inviteTokenClicked") inviteTokenClicked: Int)
