package bkh.api.entities

import java.sql.ResultSet

import bkh.db.client.DbClient
import bkh.db.client.DbClient._
import org.apache.log4j.Logger

case class SyncEndOfDayBySymbol(exchangeId: String,
                                symbol: String,
                                dateOfCall: String,
                                deltaChanges: Int,
                                deltaDatesAdded: Int,
                                deltaDatesMissing: Int,
                                numDates: Int,
                                minDate: String,
                                maxDate: String,
                                hullSize: Int,
                                biggestContiguous: Int,
                                meanContiguous: Double,
                                numHoles: Int,
                                biggestHole: Int,
                                meanHole: Double) {

  def key(): (String, String) = (exchangeId, symbol)

  def upsert(dbClient: DbClient): Unit = {
    import SyncEndOfDayBySymbol._
    val syncEndOfDayBySymbol =
      s"""insert into sync_end_of_day_by_symbol
         |(exchange_id, symbol, date_of_call,
         | delta_changes, delta_dates_added, delta_dates_missing, num_dates,
         | min_date, max_date, hull_size,
         | biggest_contiguous, mean_contiguous, num_holes, biggest_hole, mean_hole)
         |values
         |(  ${nullableString(exchangeId)},
         |   ${nullableString(symbol)},
         |   ${nullableString(dateOfCall)},
         |   ${deltaChanges},
         |   ${deltaDatesAdded},
         |   ${deltaDatesMissing},
         |   ${numDates},
         |   ${nullableString(minDate)},
         |   ${nullableString(maxDate)},
         |   ${hullSize},
         |   ${biggestContiguous},
         |   ${meanContiguous},
         |   ${numHoles},
         |   ${biggestHole},
         |   ${meanHole}
         |)
         |on conflict on constraint sync_end_of_day_by_symbol_exchange_id_symbol_pkey
         |do update set
         |  date_of_call        = excluded.date_of_call,
         |  delta_changes       = excluded.delta_changes,
         |  delta_dates_added   = excluded.delta_dates_added,
         |  delta_dates_missing = excluded.delta_dates_missing,
         |  num_dates           = excluded.num_dates,
         |  min_date            = excluded.min_date,
         |  max_date            = excluded.max_date,
         |  hull_size           = excluded.hull_size,
         |  biggest_contiguous  = excluded.biggest_contiguous,
         |  mean_contiguous     = excluded.mean_contiguous,
         |  num_holes           = excluded.num_holes,
         |  biggest_hole        = excluded.biggest_hole,
         |  mean_hole           = excluded.mean_hole
         |""".stripMargin
    val statement = dbClient.statement.get()
    try {
      statement.executeUpdate(syncEndOfDayBySymbol)
      statement.getConnection.commit()
    } catch {
      case t: Throwable =>
        val errorMsg = s"failed to insert into sync_end_of_day_by_symbol (key=${key()}):\n$syncEndOfDayBySymbol"
        logger.error(errorMsg, t)
        statement.getConnection.rollback()
        throw new RuntimeException(errorMsg, t)
    }
  }

}

object SyncEndOfDayBySymbol {

  val logger: Logger = Logger.getLogger("SyncEndOfDayBySymbol")

  def fromResultSet(rs: ResultSet): SyncEndOfDayBySymbol = {
    SyncEndOfDayBySymbol(
      rs.getString("exchange_id"),
      rs.getString("symbol"),
      rs.getString("date_of_call"),
      rs.getInt   ("delta_changes"),
      rs.getInt   ("delta_dates_added"),
      rs.getInt   ("delta_dates_missing"),
      rs.getInt   ("num_dates"),
      rs.getString("min_date"),
      rs.getString("max_date"),
      rs.getInt   ("hull_size"),
      rs.getInt   ("biggest_contiguous"),
      rs.getDouble("mean_contiguous"),
      rs.getInt   ("num_holes"),
      rs.getInt   ("biggest_hole"),
      rs.getDouble("mean_hole")
    )
  }

}
