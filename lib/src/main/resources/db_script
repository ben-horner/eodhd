-- If it matters, this is for postrgesql

-- create database eodhd;
-- \c eodhd


drop table eodhd_sync_symbol_list;
drop table eodhd_sync_exchange_list;
drop table eodhd_shares;
drop table eodhd_end_of_day;
drop table eodhd_symbol;
drop table eodhd_exchange;
vacuum full analyze;

select count(*) from eodhd_exchange; --74
select count(*) from eodhd_list_exchanges_syncing; -- 74
select count(*) from eodhd_symbol; -- 142,811
select count(*) from eodhd_list_symbols_syncing; -- 142,811
select count(*) from eodhd_end_of_day; -- 289,113,203 (incomplete, only 1 day)
select count(*) as c, exchange_id from eodhd_end_of_day group by exchange_id order by c;




create table eodhd_exchange (
  exchange_id   varchar(10)  not null,
  name          varchar(30)  not null,
  country       varchar(15)  not null,
  currency      varchar(10)  not null,
  operating_mic varchar(10)
);
alter table eodhd_exchange add constraint eodhd_exchange_exchange_id_pkey primary key (exchange_id);

create table sync_exchange_list (
  exchange_id     varchar(10) not null,
  first_seen      timestamp   not null,
  last_updated    timestamp   not null,
  last_seen       timestamp   not null,
  last_absent     timestamp,
  last_reappeared timestamp
);
alter table sync_exchange_list add constraint sync_exchange_list_exchange_id_pkey primary key (exchange_id);
alter table sync_exchange_list add constraint sync_exchange_list_exchange_id_fkey foreign key (exchange_id) references eodhd_exchange (exchange_id);




create table eodhd_symbol (
  exchange_id varchar(10)  not null,
  exchange    varchar(10),
  symbol      varchar(20)  not null,
  name        varchar(180) not null,
  country     varchar(15),
  currency    varchar(10),
  type        varchar(15)
);
alter table eodhd_symbol add constraint eodhd_symbol_exchange_id_symbol_pkey primary key (exchange_id, symbol);
alter table eodhd_symbol add constraint eodhd_symbol_exchange_id_fkey foreign key (exchange_id) references eodhd_exchange (exchange_id);

create table sync_symbol_list (
  exchange_id     varchar(10) not null,
  symbol          varchar(20) not null,
  first_seen      timestamp   not null,
  last_updated    timestamp   not null,
  last_seen       timestamp   not null,
  last_absent     timestamp,
  last_reappeared timestamp
);
alter table sync_symbol_list add constraint sync_symbol_list_exchange_id_symbol_pkey primary key (exchange_id, symbol);
alter table sync_symbol_list add constraint sync_symbol_list_exchange_id_symbol_fkey foreign key (exchange_id, symbol) references eodhd_symbol (exchange_id, symbol);




create table eodhd_end_of_day (
  exchange_id    varchar(10)    not null,
  symbol         varchar(20)    not null,
  date           date           not null,
  open           numeric(11, 4),
  high           numeric(11, 4),
  low            numeric(11, 4),
  close          numeric(11, 4),
  adjusted_close numeric(11, 4),
  volume         numeric(20, 4)
);
alter table eodhd_end_of_day add constraint eodhd_end_of_day_exchange_id_symbol_date_pkey primary key (exchange_id, symbol, date);
alter table eodhd_end_of_day add constraint eodhd_end_of_day_exchange_id_symbol_fkey foreign key (exchange_id, symbol) references eodhd_symbol (exchange_id, symbol);
alter table eodhd_end_of_day add constraint eodhd_end_of_day_date_symbol_exchange_id_unq unique (date, symbol, exchange_id);

create table eodhd_bond_end_of_day (
  exchange_id varchar(10)    not null,
  symbol      varchar(20)    not null,
  date        date           not null,
  price       numeric(11, 4),
  yield       numeric(11, 4),
  volume      numeric(20, 4)
);
alter table eodhd_bond_end_of_day add constraint eodhd_bond_end_of_day_exchange_id_symbol_date_pkey primary key (exchange_id, symbol, date);
alter table eodhd_bond_end_of_day add constraint eodhd_bond_end_of_day_exchange_id_symbol_fkey foreign key (exchange_id, symbol) references eodhd_symbol (exchange_id, symbol);
alter table eodhd_bond_end_of_day add constraint eodhd_bond_end_of_day_date_symbol_exchange_id_unq unique (date, symbol, exchange_id);

create table sync_end_of_day_by_symbol (
  exchange_id         varchar(10)   not null,
  symbol              varchar(20)   not null,
  date_of_call        date          not null, -- when api call was made (by program)
  delta_changes       integer       not null, -- number of end_of_day records with changed values
  delta_dates_added   integer       not null, -- number of dates in api call not previously in db
  delta_dates_missing integer       not null, -- number of dates in db not returned in api call
  num_dates           integer       not null, -- number of dates in api call
  min_date            date,
  max_date            date,
  hull_size           integer       not null, -- number of days in range from earliest to latest (regardless of holes)
  biggest_contiguous  integer       not null, -- size of longest contiguous run of dates returned from api (may be 5, business days in a week)
  mean_contiguous     numeric(7, 1) not null, -- mean length of contiguous runs (expecting 5, business days in a week)
  num_holes           integer       not null, -- number of holes (could be a lot for weekends)
  biggest_hole        integer       not null, -- biggest hole (looking for max > 2 as a problem, weekends are not a problem)
  mean_hole           numeric(7, 1) not null  -- mean length of contiguous missing dates (may be 2, for weekends)
);
alter table sync_end_of_day_by_symbol add constraint sync_end_of_day_by_symbol_exchange_id_symbol_pkey primary key (exchange_id, symbol);
alter table sync_end_of_day_by_symbol add constraint sync_end_of_day_by_symbol_exchange_id_symbol_fkey foreign key (exchange_id, symbol) references eodhd_symbol (exchange_id, symbol);

create table sync_end_of_day_by_date (
  exchange_id           varchar(10) not null,
  date                  date        not null,
  date_of_call          date        not null, -- when api call was made (by program)
  delta_changes         integer     not null, -- number of end_of_day records with changed values
  delta_symbols_added   integer     not null, -- number of symbols in api call not previously in db
  delta_symbols_missing integer     not null, -- number of symbols in db not returned in api call
  num_symbols           integer     not null  -- number of symbols in api call
);
alter table sync_end_of_day_by_date add constraint sync_end_of_day_by_date_exchange_id_date_pkey primary key (exchange_id, date);
alter table sync_end_of_day_by_date add constraint sync_end_of_day_by_date_exchange_id_fkey foreign key (exchange_id) references eodhd_exchange (exchange_id);




create table eodhd_shares (
  exchange_id    varchar(10)    not null,
  symbol         varchar(20)    not null,
  date           date           not null,
  shares         numeric(20, 4) not null
);
alter table eodhd_shares add constraint eodhd_shares_exchange_id_symbol_date_pkey primary key (exchange_id, symbol, date);
alter table eodhd_shares add constraint eodhd_shares_exchange_id_symbol_date_fkey foreign key (exchange_id, symbol) references eodhd_symbol (exchange_id, symbol);
alter table eodhd_shares add constraint eodhd_shares_date_symbol_exchange_id_unq unique (date, symbol, exchange_id);
